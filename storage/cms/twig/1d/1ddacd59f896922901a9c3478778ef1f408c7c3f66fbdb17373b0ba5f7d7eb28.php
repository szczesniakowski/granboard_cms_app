<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/partials/navigation.htm */
class __TwigTemplate_1dad57e8a659c756170498cb3e551c9d185e9cd8384c8bae364ca85a8206e99e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["products"] = twig_get_attribute($this->env, $this->source, ($context["productList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["services"] = twig_get_attribute($this->env, $this->source, ($context["serviceList"] ?? null), "records", [], "any", false, false, false, 2);
        // line 3
        echo "
<nav class=\"navigation\">
    <a href=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, ($context["staticMenu"] ?? null), "menuItems", [], "any", false, false, false, 5)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "url", [], "any", false, false, false, 5), "html", null, true);
        echo "\">
        <img class=\"navLogo\" src=\"";
        // line 6
        echo $this->extensions['System\Twig\Extension']->mediaFilter("logo_white.png");
        echo "\" alt=\"\">
        <img class=\"navLogo_product\" src=\"";
        // line 7
        echo $this->extensions['System\Twig\Extension']->mediaFilter("logo_color.png");
        echo "\" alt=\"\">
    </a>
    <div class=\"navList\">
        <ul id=\"navihationList\" class=\"navList_pagesList\">
            ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["staticMenu"] ?? null), "menuItems", [], "any", false, false, false, 11));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 12
            echo "            ";
            if (($context["i"] > 0)) {
                // line 13
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 13) != "Zamów produkt")) {
                    // line 14
                    echo "            <li class=\"page";
                    if (twig_get_attribute($this->env, $this->source, $context["item"], "isActive", [], "any", false, false, false, 14)) {
                        echo " active ";
                    }
                    if (((twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 14) == "Produkty") || (twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 14) == "Usługi"))) {
                        echo " subPageList ";
                    }
                    echo "\"
                data-id=\"\">
                <a href=\"";
                    // line 16
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, false, 16), "html", null, true);
                    echo "\" class=\"page_link\">
                    ";
                    // line 17
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 17), "html", null, true);
                    echo "
                </a>

                ";
                    // line 20
                    if ((twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 20) == "Produkty")) {
                        // line 21
                        echo "                <div id=\"productList\" class=\"nav_productList\">
                    <div class=\"nav_productList_content\">
                        <div class=\"nav_productList_title\">
                            Zrównoważone rozwiązania na bazie drewna
                        </div>
                        <div class=\"nav_product_list \">
                            ";
                        // line 27
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["productList"] ?? null), "records", [], "any", false, false, false, 27));
                        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                            // line 28
                            echo "                            ";
                            if (twig_get_attribute($this->env, $this->source, $context["item"], "show", [], "any", false, false, false, 28)) {
                                // line 29
                                echo "                            <a href=\"produkt/";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 29), "html", null, true);
                                echo "\"
                               class=\"nav_product_item ";
                                // line 30
                                if ((twig_length_filter($this->env, ($context["records"] ?? null)) > 4)) {
                                    echo " halfList ";
                                }
                                echo "\"
                               data-product=\"";
                                // line 31
                                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                                echo "\">";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 31), "html", null, true);
                                echo "</a>
                            ";
                            }
                            // line 33
                            echo "                            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 34
                        echo "                        </div>
                    </div>
                    <div class=\"nav_productList_img\">
                        ";
                        // line 37
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["productList"] ?? null), "records", [], "any", false, false, false, 37));
                        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                            // line 38
                            echo "                        <img class=\"nav_product_img_item ";
                            if (($context["i"] == 0)) {
                                echo " active ";
                            }
                            echo "\" data-product=\"";
                            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                            echo "\"
                             src=\"";
                            // line 39
                            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "main_img", [], "any", false, false, false, 39));
                            echo "\"
                             alt=\"\">
                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 42
                        echo "                    </div>
                </div>
                ";
                    }
                    // line 45
                    echo "                ";
                    if ((twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 45) == "Usługi")) {
                        // line 46
                        echo "                <div id=\"serviceList\" class=\"nav_productList\">
                    <div class=\"nav_productList_content\">
                        <div class=\"nav_productList_title\">
                            Zrównoważone rozwiązania na bazie drewna
                        </div>
                        <div class=\"nav_product_list \">
                            ";
                        // line 52
                        if (((twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 52) == "Usługi") && twig_get_attribute($this->env, $this->source, $context["item"], "isActive", [], "any", false, false, false, 52))) {
                            // line 53
                            echo "                            ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["serviceList"] ?? null), "records", [], "any", false, false, false, 53));
                            foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                                // line 54
                                echo "                            <p onclick=\"scroll_set_activeService('";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 54), "html", null, true);
                                echo "')\"
                               class=\"nav_product_item ";
                                // line 55
                                if ((twig_length_filter($this->env, ($context["records"] ?? null)) > 4)) {
                                    echo " halfList ";
                                }
                                echo "\"
                               data-service=\"";
                                // line 56
                                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                                echo "\">
                                ";
                                // line 57
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 57), "html", null, true);
                                echo "
                            </p>
                            ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 60
                            echo "                            ";
                        } else {
                            // line 61
                            echo "                            ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["serviceList"] ?? null), "records", [], "any", false, false, false, 61));
                            foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                                // line 62
                                echo "                            <a href=\"uslugi#";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 62), "html", null, true);
                                echo "\"
                               class=\"nav_product_item ";
                                // line 63
                                if ((twig_length_filter($this->env, ($context["records"] ?? null)) > 4)) {
                                    echo " halfList ";
                                }
                                echo "\"
                               data-service=\"";
                                // line 64
                                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                                echo "\">
                                ";
                                // line 65
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 65), "html", null, true);
                                echo "
                            </a>
                            ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 68
                            echo "
                            ";
                        }
                        // line 70
                        echo "                        </div>
                    </div>
                    <div class=\"nav_productList_img\">
                        ";
                        // line 73
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["serviceList"] ?? null), "records", [], "any", false, false, false, 73));
                        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                            // line 74
                            echo "                        <img class=\"nav_product_img_item ";
                            if (($context["i"] == 0)) {
                                echo " active ";
                            }
                            echo "\" data-service=\"";
                            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                            echo "\"
                             src=\"";
                            // line 75
                            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "img", [], "any", false, false, false, 75));
                            echo "\"
                             alt=\"\">
                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 78
                        echo "                    </div>
                </div>
                ";
                    }
                    // line 81
                    echo "            </li>
            ";
                }
                // line 83
                echo "            ";
            }
            // line 84
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "            <!--            <li class=\"navList_languages\">-->
            <!--                EN-->
            <!--            </li>-->
            <li id=\"nav_burder_close\" class=\"nav_burder nav_burder_close\">
                <span></span><span></span><span></span>
            </li>
        </ul>

        <div id=\"nav_burder\" class=\"nav_burder\">
            <span></span><span></span><span></span>
        </div>

        ";
        // line 97
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["staticMenu"] ?? null), "menuItems", [], "any", false, false, false, 97));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 98
            echo "        ";
            if ((twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 98) == "Zamów produkt")) {
                // line 99
                echo "        <a class=\"getOrder\">
            Zamów produkt
        </a>
        ";
            }
            // line 103
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "    </div>
</nav>

<script>
    const nav_productList = document.getElementById('productList');
    const nav_serviceList = document.getElementById('serviceList');

    const productListHeight = nav_productList.clientHeight;
    const serviceListHeight = nav_serviceList.clientHeight;

    nav_productList.setAttribute('data-height', productListHeight);
    nav_serviceList.setAttribute('data-height', serviceListHeight);

    nav_productList.style.height = 0;
    nav_serviceList.style.height = 0;

    const burger = document.getElementById('nav_burder');
    const burgerClose = document.getElementById('nav_burder_close');
    const navigation = document.getElementById('navihationList');

    burger.addEventListener('click', function (e) {
        burger.classList.add('active');
        burgerClose.classList.add('active');
        navigation.classList.add('active');
    });

    burgerClose.addEventListener('click', function () {
        burger.classList.remove('active');
        burgerClose.classList.remove('active');
        navigation.classList.remove('active');
    });

    \$(document).ready(function () {

        \$('.subPageList').on('mouseover', function () {
            \$('.nav_productList').css({height: 0});
            const elHeight = \$(this).find('.nav_productList').attr('data-height');
            \$(this).find('.nav_productList').css({height: elHeight + 'px'});

            \$('.nav_product_item').addClass('active');
            \$('.nav_product_img_item').addClass('active');

            \$('.nav_product_item[data-product=\"0\"]').addClass('active');
            \$('.nav_product_item[data-service=\"0\"]').addClass('active');

            \$('.nav_product_img_item[data-service=\"0\"]').addClass('active');
            \$('.nav_product_img_item[data-product=\"0\"]').addClass('active');
        });

        \$('.subPageList').on('mouseleave', function () {
            \$('.nav_productList').css({height: 0});


        });

        \$('.nav_product_item[data-service]').on('mouseover', function () {
            const target = \$(this).attr('data-service');
            // \$('.nav_product_img_item[data-service]').removeClass('active');
            \$('.nav_product[data-service]').removeClass('active');

            \$(this).addClass('active');
            \$('.nav_product_img_item[data-service=\"' + target + '\"]').addClass('active');
        });

        \$('.nav_product_item[data-product]').on('mouseover', function () {
            const target = \$(this).attr('data-product');
            // \$('.nav_product_img_item[data-product]').removeClass('active');
            \$('.nav_product[data-product]').removeClass('active');

            \$(this).addClass('active');
            \$('.nav_product_img_item[data-product=\"' + target + '\"]').addClass('active');
        });
    });

    // const pageProduct = document.getElementById('page-product');
    //
    // pageProduct.addEventListener('mouseover', function (el) {
    //     nav_productList.classList.add('active');
    //     console.log(navHeight);
    //     nav_productList.style.height = navHeight + 'px'
    // });
    //
    // pageProduct.addEventListener('mouseout', function () {
    //     nav_productList.classList.remove('active');
    //     nav_productList.style.height = 0;
    // });

    const nav_product_item = Array.from(document.querySelectorAll('.nav_product_item'));
    const nav_product_img = Array.from(document.querySelectorAll('.nav_product_img_item'));

    nav_product_item.forEach(function (el) {
        el.addEventListener('mouseover', function (e) {

            target = e.target.closest('.nav_product_item').getAttribute('data-product');
            nav_product_img.forEach(function (item) {
                item.classList.remove('active');
            });
            document.querySelector('.nav_product_img_item[data-product=\"' + target + '\"]').classList.add('active');
        });
    });


</script>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/partials/navigation.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  320 => 104,  314 => 103,  308 => 99,  305 => 98,  301 => 97,  287 => 85,  281 => 84,  278 => 83,  274 => 81,  269 => 78,  260 => 75,  251 => 74,  247 => 73,  242 => 70,  238 => 68,  229 => 65,  225 => 64,  219 => 63,  214 => 62,  209 => 61,  206 => 60,  197 => 57,  193 => 56,  187 => 55,  182 => 54,  177 => 53,  175 => 52,  167 => 46,  164 => 45,  159 => 42,  150 => 39,  141 => 38,  137 => 37,  132 => 34,  126 => 33,  119 => 31,  113 => 30,  108 => 29,  105 => 28,  101 => 27,  93 => 21,  91 => 20,  85 => 17,  81 => 16,  70 => 14,  67 => 13,  64 => 12,  60 => 11,  53 => 7,  49 => 6,  45 => 5,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set products = productList.records %}
{% set services = serviceList.records %}

<nav class=\"navigation\">
    <a href=\"{{staticMenu.menuItems[0].url}}\">
        <img class=\"navLogo\" src=\"{{ 'logo_white.png' | media}}\" alt=\"\">
        <img class=\"navLogo_product\" src=\"{{ 'logo_color.png' | media}}\" alt=\"\">
    </a>
    <div class=\"navList\">
        <ul id=\"navihationList\" class=\"navList_pagesList\">
            {% for i,item in staticMenu.menuItems %}
            {% if(i>0) %}
            {% if(item.title!='Zamów produkt') %}
            <li class=\"page{% if(item.isActive) %} active {% endif %}{% if(item.title == 'Produkty' or item.title == 'Usługi') %} subPageList {% endif %}\"
                data-id=\"\">
                <a href=\"{{item.url}}\" class=\"page_link\">
                    {{item.title}}
                </a>

                {% if(item.title=='Produkty') %}
                <div id=\"productList\" class=\"nav_productList\">
                    <div class=\"nav_productList_content\">
                        <div class=\"nav_productList_title\">
                            Zrównoważone rozwiązania na bazie drewna
                        </div>
                        <div class=\"nav_product_list \">
                            {% for i,item in productList.records %}
                            {% if(item.show) %}
                            <a href=\"produkt/{{ item.slug }}\"
                               class=\"nav_product_item {% if(records|length > 4) %} halfList {% endif %}\"
                               data-product=\"{{i}}\">{{ item.name }}</a>
                            {% endif %}
                            {% endfor %}
                        </div>
                    </div>
                    <div class=\"nav_productList_img\">
                        {% for i,item in productList.records %}
                        <img class=\"nav_product_img_item {% if(i==0) %} active {% endif %}\" data-product=\"{{i}}\"
                             src=\"{{ item.main_img | media}}\"
                             alt=\"\">
                        {% endfor %}
                    </div>
                </div>
                {% endif %}
                {% if(item.title=='Usługi') %}
                <div id=\"serviceList\" class=\"nav_productList\">
                    <div class=\"nav_productList_content\">
                        <div class=\"nav_productList_title\">
                            Zrównoważone rozwiązania na bazie drewna
                        </div>
                        <div class=\"nav_product_list \">
                            {% if(item.title == 'Usługi' and item.isActive) %}
                            {% for i,item in serviceList.records %}
                            <p onclick=\"scroll_set_activeService('{{item.slug}}')\"
                               class=\"nav_product_item {% if(records|length > 4) %} halfList {% endif %}\"
                               data-service=\"{{i}}\">
                                {{ item.name }}
                            </p>
                            {% endfor %}
                            {% else %}
                            {% for i,item in serviceList.records %}
                            <a href=\"uslugi#{{item.slug}}\"
                               class=\"nav_product_item {% if(records|length > 4) %} halfList {% endif %}\"
                               data-service=\"{{i}}\">
                                {{ item.name }}
                            </a>
                            {% endfor %}

                            {% endif %}
                        </div>
                    </div>
                    <div class=\"nav_productList_img\">
                        {% for i,item in serviceList.records %}
                        <img class=\"nav_product_img_item {% if(i==0) %} active {% endif %}\" data-service=\"{{i}}\"
                             src=\"{{ item.img | media}}\"
                             alt=\"\">
                        {% endfor %}
                    </div>
                </div>
                {% endif %}
            </li>
            {% endif %}
            {% endif %}
            {% endfor %}
            <!--            <li class=\"navList_languages\">-->
            <!--                EN-->
            <!--            </li>-->
            <li id=\"nav_burder_close\" class=\"nav_burder nav_burder_close\">
                <span></span><span></span><span></span>
            </li>
        </ul>

        <div id=\"nav_burder\" class=\"nav_burder\">
            <span></span><span></span><span></span>
        </div>

        {% for item in staticMenu.menuItems %}
        {% if(item.title=='Zamów produkt') %}
        <a class=\"getOrder\">
            Zamów produkt
        </a>
        {% endif %}
        {% endfor %}
    </div>
</nav>

<script>
    const nav_productList = document.getElementById('productList');
    const nav_serviceList = document.getElementById('serviceList');

    const productListHeight = nav_productList.clientHeight;
    const serviceListHeight = nav_serviceList.clientHeight;

    nav_productList.setAttribute('data-height', productListHeight);
    nav_serviceList.setAttribute('data-height', serviceListHeight);

    nav_productList.style.height = 0;
    nav_serviceList.style.height = 0;

    const burger = document.getElementById('nav_burder');
    const burgerClose = document.getElementById('nav_burder_close');
    const navigation = document.getElementById('navihationList');

    burger.addEventListener('click', function (e) {
        burger.classList.add('active');
        burgerClose.classList.add('active');
        navigation.classList.add('active');
    });

    burgerClose.addEventListener('click', function () {
        burger.classList.remove('active');
        burgerClose.classList.remove('active');
        navigation.classList.remove('active');
    });

    \$(document).ready(function () {

        \$('.subPageList').on('mouseover', function () {
            \$('.nav_productList').css({height: 0});
            const elHeight = \$(this).find('.nav_productList').attr('data-height');
            \$(this).find('.nav_productList').css({height: elHeight + 'px'});

            \$('.nav_product_item').addClass('active');
            \$('.nav_product_img_item').addClass('active');

            \$('.nav_product_item[data-product=\"0\"]').addClass('active');
            \$('.nav_product_item[data-service=\"0\"]').addClass('active');

            \$('.nav_product_img_item[data-service=\"0\"]').addClass('active');
            \$('.nav_product_img_item[data-product=\"0\"]').addClass('active');
        });

        \$('.subPageList').on('mouseleave', function () {
            \$('.nav_productList').css({height: 0});


        });

        \$('.nav_product_item[data-service]').on('mouseover', function () {
            const target = \$(this).attr('data-service');
            // \$('.nav_product_img_item[data-service]').removeClass('active');
            \$('.nav_product[data-service]').removeClass('active');

            \$(this).addClass('active');
            \$('.nav_product_img_item[data-service=\"' + target + '\"]').addClass('active');
        });

        \$('.nav_product_item[data-product]').on('mouseover', function () {
            const target = \$(this).attr('data-product');
            // \$('.nav_product_img_item[data-product]').removeClass('active');
            \$('.nav_product[data-product]').removeClass('active');

            \$(this).addClass('active');
            \$('.nav_product_img_item[data-product=\"' + target + '\"]').addClass('active');
        });
    });

    // const pageProduct = document.getElementById('page-product');
    //
    // pageProduct.addEventListener('mouseover', function (el) {
    //     nav_productList.classList.add('active');
    //     console.log(navHeight);
    //     nav_productList.style.height = navHeight + 'px'
    // });
    //
    // pageProduct.addEventListener('mouseout', function () {
    //     nav_productList.classList.remove('active');
    //     nav_productList.style.height = 0;
    // });

    const nav_product_item = Array.from(document.querySelectorAll('.nav_product_item'));
    const nav_product_img = Array.from(document.querySelectorAll('.nav_product_img_item'));

    nav_product_item.forEach(function (el) {
        el.addEventListener('mouseover', function (e) {

            target = e.target.closest('.nav_product_item').getAttribute('data-product');
            nav_product_img.forEach(function (item) {
                item.classList.remove('active');
            });
            document.querySelector('.nav_product_img_item[data-product=\"' + target + '\"]').classList.add('active');
        });
    });


</script>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/partials/navigation.htm", "");
    }
}
