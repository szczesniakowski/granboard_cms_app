<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/partials/footer.htm */
class __TwigTemplate_1843ee7f6c16a93f82921e7fefcbda29b73b2cd1a4f4b4268b204e933eda59be extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 3);
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 4);
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 5);
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 6);
        // line 7
        echo "
";
        // line 8
        $context["recordsService"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "records", [], "any", false, false, false, 8);
        // line 9
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "displayColumn", [], "any", false, false, false, 9);
        // line 10
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "noRecordsMessage", [], "any", false, false, false, 10);
        // line 11
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsPage", [], "any", false, false, false, 11);
        // line 12
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 12);
        // line 13
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 13);
        // line 14
        echo "
<footer class=\"footer\">

    <div class=\"container\">
        <div class=\"footer_contact\">
            <img src=\"";
        // line 19
        echo $this->extensions['System\Twig\Extension']->mediaFilter("logo_color.png");
        echo "\" alt=\"\">
            <div class=\"content\">
                <p>GranBoard Sp. z o.o. </p>
                <p>ul. Naftowa 1, Granowiec 63-435, Poland</p>
                <p>Tel: (+48) 535 820 620</p>
                <p>Pon - Pt: 8:00 - 16:00</p>
            </div>
        </div>
        <div class=\"footer_pageMap\">
            <div class=\"footer_page\">
                <a href=\"produkty\" class=\"footer_pageMap_title\">Produkty</a>
                ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 31
            echo "                ";
            if (twig_get_attribute($this->env, $this->source, $context["record"], "show", [], "any", false, false, false, 31)) {
                // line 32
                echo "                <a href=\"produkty/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "slug", [], "any", false, false, false, 32), "html", null, true);
                echo "\" class=\"footer_pageMap_page\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "name", [], "any", false, false, false, 32), "html", null, true);
                echo "</a>
                ";
            }
            // line 34
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            </div>
            <div class=\"footer_page\">
                <a href=\"uslugi\" class=\"footer_pageMap_title\">Usługi</a>
                ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["recordsService"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 39
            echo "                <a href=\"uslugi#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "slug", [], "any", false, false, false, 39), "html", null, true);
            echo "\" class=\"footer_pageMap_page\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "name", [], "any", false, false, false, 39), "html", null, true);
            echo "</a>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "            </div>
            <div class=\"footer_page\">
                <a href=\"kariera\" class=\"footer_pageMap_title\">Kariera</a>
                <a href=\"kariera\" class=\"footer_pageMap_page\">Oferty pracy</a>
            </div>
            <div class=\"footer_page\">
                <a href=\"kontakt\" class=\"footer_pageMap_title\">Kontakt</a>
                <a href=\"kontakt\" class=\"footer_pageMap_page\">Dane kontaktowe</a>
                <a href=\"kontakt\" class=\"footer_pageMap_page\">Mapa dojazdu</a>
                <a href=\"kontakt\" class=\"footer_pageMap_page\">Formularz kontaktowy</a>
            </div>
        </div>
    </div>
    <div class=\"containerFull\">
        <p>© Copyright 2020 · GranBoard Sp. z o.o. · All rights reserved</p>
        <div class=\"links\">
            <a href=\"\">Polityka prywatności</a>
            <a href=\"\">Cookies</a>
        </div>
    </div>
</footer>

<script src=\"";
        // line 63
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/animationScript.js");
        echo "\"></script>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 63,  126 => 41,  115 => 39,  111 => 38,  106 => 35,  100 => 34,  92 => 32,  89 => 31,  85 => 30,  71 => 19,  64 => 14,  62 => 13,  60 => 12,  58 => 11,  56 => 10,  54 => 9,  52 => 8,  49 => 7,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

{% set recordsService = builderList2.records %}
{% set displayColumn = builderList2.displayColumn %}
{% set noRecordsMessage = builderList2.noRecordsMessage %}
{% set detailsPage = builderList2.detailsPage %}
{% set detailsKeyColumn = builderList2.detailsKeyColumn %}
{% set detailsUrlParameter = builderList2.detailsUrlParameter %}

<footer class=\"footer\">

    <div class=\"container\">
        <div class=\"footer_contact\">
            <img src=\"{{ 'logo_color.png' | media }}\" alt=\"\">
            <div class=\"content\">
                <p>GranBoard Sp. z o.o. </p>
                <p>ul. Naftowa 1, Granowiec 63-435, Poland</p>
                <p>Tel: (+48) 535 820 620</p>
                <p>Pon - Pt: 8:00 - 16:00</p>
            </div>
        </div>
        <div class=\"footer_pageMap\">
            <div class=\"footer_page\">
                <a href=\"produkty\" class=\"footer_pageMap_title\">Produkty</a>
                {% for record in records %}
                {% if(record.show) %}
                <a href=\"produkty/{{ record.slug }}\" class=\"footer_pageMap_page\">{{record.name}}</a>
                {% endif %}
                {% endfor %}
            </div>
            <div class=\"footer_page\">
                <a href=\"uslugi\" class=\"footer_pageMap_title\">Usługi</a>
                {% for record in recordsService %}
                <a href=\"uslugi#{{record.slug}}\" class=\"footer_pageMap_page\">{{record.name}}</a>
                {% endfor %}
            </div>
            <div class=\"footer_page\">
                <a href=\"kariera\" class=\"footer_pageMap_title\">Kariera</a>
                <a href=\"kariera\" class=\"footer_pageMap_page\">Oferty pracy</a>
            </div>
            <div class=\"footer_page\">
                <a href=\"kontakt\" class=\"footer_pageMap_title\">Kontakt</a>
                <a href=\"kontakt\" class=\"footer_pageMap_page\">Dane kontaktowe</a>
                <a href=\"kontakt\" class=\"footer_pageMap_page\">Mapa dojazdu</a>
                <a href=\"kontakt\" class=\"footer_pageMap_page\">Formularz kontaktowy</a>
            </div>
        </div>
    </div>
    <div class=\"containerFull\">
        <p>© Copyright 2020 · GranBoard Sp. z o.o. · All rights reserved</p>
        <div class=\"links\">
            <a href=\"\">Polityka prywatności</a>
            <a href=\"\">Cookies</a>
        </div>
    </div>
</footer>

<script src=\"{{ 'assets/js/animationScript.js' | theme }}\"></script>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/partials/footer.htm", "");
    }
}
