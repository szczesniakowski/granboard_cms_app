<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/layouts/career.htm */
class __TwigTemplate_ce3f417729d2f689745b0fdc85d903dc204171ae8b86b9e8ded4814c6dde57b1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "











";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["extraData"] ?? null), "benefits", [], "any", false, false, false, 13));
        foreach ($context['_seq'] as $context["_key"] => $context["fields"]) {
            // line 14
            echo "


";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "








";
        // line 27
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["jobList"] ?? null), "records", [], "any", false, false, false, 27);
        // line 28
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["jobList"] ?? null), "displayColumn", [], "any", false, false, false, 28);
        // line 29
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["jobList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 29);
        // line 30
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["jobList"] ?? null), "detailsPage", [], "any", false, false, false, 30);
        // line 31
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["jobList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 31);
        // line 32
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["jobList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 32);
        // line 33
        echo "
<body class=\"workOffers\">

";
        // line 36
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 37
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 38
        echo "
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"";
        // line 40
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["hero_img"] ?? null));
        echo "\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"fadeIn\">
                Kariera
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                ";
        // line 49
        echo ($context["hero_mainText"] ?? null);
        echo "
            </h1>
        </div>
    </div>
</section>
<section class=\"simple\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"slideFromLeft\">
            ";
        // line 57
        echo twig_escape_filter($this->env, ($context["text_1_title"] ?? null), "html", null, true);
        echo "
        </h3>
        <p class=\"text\" data-animation=\"slideFromRight\">
            ";
        // line 60
        echo twig_escape_filter($this->env, ($context["text_1_text"] ?? null), "html", null, true);
        echo "
        </p>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container topContent\">
        <div class=\"row\">
            <div class=\"col\">
                <div class=\"textContentTop\">
                    <div class=\"textContent\">
                        <h3 class=\"title whiteTitle\" data-animation=\"slideFromLeft\">
                            ";
        // line 71
        echo twig_escape_filter($this->env, ($context["imgText_1_title"] ?? null), "html", null, true);
        echo "
                        </h3>
                        <p class=\"text topText\" data-animation=\"slideFromLeft\">
                            ";
        // line 74
        echo twig_escape_filter($this->env, ($context["imgText_1_text"] ?? null), "html", null, true);
        echo "
                        </p>
                        <div class=\"arrow_btn_bg scroll_btn\" href=\"\" data-animation=\"slideFromLeft\">Zobacz OFERTY PRACY
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col\">
                <img src=\"";
        // line 82
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["imgText_1_img"] ?? null));
        echo "\" alt=\"\" data-animation=\"slideFromRight\">
            </div>
        </div>
    </div>
</section>
<section class=\"interestingInfo\">
    <div class=\"container\">
        <p class=\"title tableTitle\" data-animation=\"fadeIn\">
            ";
        // line 90
        echo twig_escape_filter($this->env, ($context["benefits_title"] ?? null), "html", null, true);
        echo "
        </p>
        <div class=\"list\">
            ";
        // line 93
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["benefits"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 94
            echo "            <div class=\"adress\">
                <div class=\"icon\">
                    <img class=\"\" src=\"";
            // line 96
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "img", [], "any", false, false, false, 96));
            echo "\" alt=\"\" data-animation=\"fadeIn\">
                </div>
                <div class=\"content\">
                    <div class=\"tit\" data-animation=\"fadeIn\">
                        ";
            // line 100
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 100), "html", null, true);
            echo "
                    </div>
                    <div class=\"greyBar\" data-animation=\"fadeIn\"></div>
                    <div class=\"description\" data-animation=\"fadeIn\">
                        ";
            // line 104
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "text", [], "any", false, false, false, 104), "html", null, true);
            echo "
                    </div>
                </div>
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 109
        echo "            <div class=\"btn_scroll scroll_btn\" data-animation=\"fadeIn\">
                ZOBACZ OFERTY PRACY
            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"";
        // line 119
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["imgText_2_img"] ?? null));
        echo "\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        ";
        // line 124
        echo twig_escape_filter($this->env, ($context["imgText_2_title"] ?? null), "html", null, true);
        echo "
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        ";
        // line 127
        echo twig_escape_filter($this->env, ($context["imgText_2_text"] ?? null), "html", null, true);
        echo "
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
";
        // line 134
        if ((twig_length_filter($this->env, ($context["records"] ?? null)) > 0)) {
            // line 135
            echo "<section class=\"simple offers\">
    <div class=\"container\">

        <h3 class=\"title\" data-animation=\"fadeIn\">
            Aktualne Oferty Pracy
        </h3>
        <div class=\"offerInfo_list\">
            ";
            // line 142
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
            foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                // line 143
                echo "            <div class=\"offerInfo_el\" data-animation=\"fadeIn\">
                <div class=\"offerInfo\">
                    <div class=\"offerInfoInside\">
                        <h4>";
                // line 146
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 146), "html", null, true);
                echo "</h4>
                        <p>";
                // line 147
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "section", [], "any", false, false, false, 147), "html", null, true);
                echo "</p>
                    </div>
                    <div class=\"locationInfo\">
                        <img src=\"public/img/workOffers/location.svg\"/>
                        <p class=\"locationText\">";
                // line 151
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "location", [], "any", false, false, false, 151), "html", null, true);
                echo "</p>
                    </div>
                    <img class=\"dropdown\" data-id=\"";
                // line 153
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" src=\"";
                echo $this->extensions['System\Twig\Extension']->mediaFilter("career/arrow_down.svg");
                echo "\"/>
                </div>
                <div class=\"offer-content\" data-id=\"";
                // line 155
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\">
                    ";
                // line 156
                if (twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 156)) {
                    // line 157
                    echo "                    <h4 class=\"offerTitle\">Opis oferty</h4>
                    <p class=\"text offerText\">
                        ";
                    // line 159
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 159), "html", null, true);
                    echo "
                    </p>
                    ";
                }
                // line 162
                echo "                    ";
                if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "expectations", [], "any", false, false, false, 162)) > 0)) {
                    // line 163
                    echo "                    <h4 class=\"offerTitle\">Czego oczekujemy</h4>
                    <ul class=\"offerList\">
                        ";
                    // line 165
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "expectations", [], "any", false, false, false, 165));
                    foreach ($context['_seq'] as $context["i"] => $context["el"]) {
                        // line 166
                        echo "                        <li><img src=\"";
                        echo $this->extensions['System\Twig\Extension']->mediaFilter("career/yes.svg");
                        echo "\"/>
                            <p>";
                        // line 167
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["el"], "text1", [], "any", false, false, false, 167), "html", null, true);
                        echo "</p>
                        </li>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['i'], $context['el'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 170
                    echo "                    </ul>
                    ";
                }
                // line 172
                echo "                    ";
                if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "offer", [], "any", false, false, false, 172)) > 0)) {
                    // line 173
                    echo "                    <h4 class=\"offerTitle\">Co oferujemy</h4>
                    <ul class=\"offerList\">
                        ";
                    // line 175
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "offer", [], "any", false, false, false, 175));
                    foreach ($context['_seq'] as $context["i"] => $context["el"]) {
                        // line 176
                        echo "                        <li><img src=\"";
                        echo $this->extensions['System\Twig\Extension']->mediaFilter("career/yes.svg");
                        echo "\"/>
                            <p>";
                        // line 177
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["el"], "text1", [], "any", false, false, false, 177), "html", null, true);
                        echo "</p>
                        </li>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['i'], $context['el'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 180
                    echo "                    </ul>
                    ";
                }
                // line 182
                echo "                    <a class=\"arrow_btn_bg arrow_apply\" href=\"kariera/formularz\">
                        Aplikuj na stanowisko
                    </a>

                </div>
            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 189
            echo "        </div>

    </div>
</section>
<section class=\"simpleWithBtn offerWork\">
    <div class=\"container\">
        <h3 class=\"title\">
            ";
            // line 196
            echo twig_escape_filter($this->env, ($context["text_2_title"] ?? null), "html", null, true);
            echo "
        </h3>
        <p class=\"text\">
            ";
            // line 199
            echo twig_escape_filter($this->env, ($context["text_2_text"] ?? null), "html", null, true);
            echo "
        </p>
        <a href=\"kariera/formularz\" class=\"arrow_btn_bg\">
            Napisz do nas
        </a>
    </div>
</section>
";
        } else {
            // line 207
            echo "<section class=\"simpleWithBtn offerWork\">
    <div class=\"container\">
        <h3 class=\"title\">
            Aktualnie brak ofert pracy
        </h3>
        <p class=\"text\">
            Chcesz z nami pracowć? Napisz do nas wyślij nam dwoje CV a by na pewno je rozpatrzymy!
        </p>
        <a href=\"kariera/formularz\" class=\"arrow_btn_bg\">
            Napisz do nas
        </a>
    </div>
</section>
";
        }
        // line 221
        echo "
";
        // line 222
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 223
        echo "<script>

    const offerContent = Array.from(document.querySelectorAll('.offer-content'));

    offerContent.forEach(function (el) {
        const elHeight = el.offsetHeight;
        el.setAttribute('data-height', elHeight);
        el.style.height = \"0px\";
    });

    \$(document).ready(function () {

        \$('.scroll_btn').on('click', function () {
            \$('body,html').animate({
                scrollTop: \$('.simple.offers').offset().top
            }, 1000);
        });

        \$('.dropdown').on('click', function () {
            const target = \$(this).attr('data-id');
            const isActive = \$(this).hasClass('active');

            \$('.dropdown').removeClass('active');
            \$('.offer-content').css({'height': '0px'});

            if (!isActive) {
                \$(this).addClass('active');
                const elHeight = \$('.offer-content[data-id=\"' + target + '\"]').attr('data-height');
                console.log('elHeight', elHeight);
                \$('.offer-content[data-id=\"' + target + '\"]').css({height: elHeight + 'px'});


            }
        });
    });
</script>
</body>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/career.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  428 => 223,  424 => 222,  421 => 221,  405 => 207,  394 => 199,  388 => 196,  379 => 189,  367 => 182,  363 => 180,  354 => 177,  349 => 176,  345 => 175,  341 => 173,  338 => 172,  334 => 170,  325 => 167,  320 => 166,  316 => 165,  312 => 163,  309 => 162,  303 => 159,  299 => 157,  297 => 156,  293 => 155,  286 => 153,  281 => 151,  274 => 147,  270 => 146,  265 => 143,  261 => 142,  252 => 135,  250 => 134,  240 => 127,  234 => 124,  226 => 119,  214 => 109,  203 => 104,  196 => 100,  189 => 96,  185 => 94,  181 => 93,  175 => 90,  164 => 82,  153 => 74,  147 => 71,  133 => 60,  127 => 57,  116 => 49,  104 => 40,  100 => 38,  96 => 37,  92 => 36,  87 => 33,  85 => 32,  83 => 31,  81 => 30,  79 => 29,  77 => 28,  75 => 27,  64 => 18,  55 => 14,  51 => 13,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("











{% for fields in extraData.benefits %}



{% endfor %}









{% set records = jobList.records %}
{% set displayColumn = jobList.displayColumn %}
{% set noRecordsMessage = jobList.noRecordsMessage %}
{% set detailsPage = jobList.detailsPage %}
{% set detailsKeyColumn = jobList.detailsKeyColumn %}
{% set detailsUrlParameter = jobList.detailsUrlParameter %}

<body class=\"workOffers\">

{% partial 'header' %}
{% partial 'navigation' %}

<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"{{ hero_img | media}}\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"fadeIn\">
                Kariera
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                {{hero_mainText | raw }}
            </h1>
        </div>
    </div>
</section>
<section class=\"simple\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"slideFromLeft\">
            {{text_1_title}}
        </h3>
        <p class=\"text\" data-animation=\"slideFromRight\">
            {{text_1_text}}
        </p>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container topContent\">
        <div class=\"row\">
            <div class=\"col\">
                <div class=\"textContentTop\">
                    <div class=\"textContent\">
                        <h3 class=\"title whiteTitle\" data-animation=\"slideFromLeft\">
                            {{imgText_1_title}}
                        </h3>
                        <p class=\"text topText\" data-animation=\"slideFromLeft\">
                            {{imgText_1_text}}
                        </p>
                        <div class=\"arrow_btn_bg scroll_btn\" href=\"\" data-animation=\"slideFromLeft\">Zobacz OFERTY PRACY
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col\">
                <img src=\"{{ imgText_1_img | media }}\" alt=\"\" data-animation=\"slideFromRight\">
            </div>
        </div>
    </div>
</section>
<section class=\"interestingInfo\">
    <div class=\"container\">
        <p class=\"title tableTitle\" data-animation=\"fadeIn\">
            {{benefits_title}}
        </p>
        <div class=\"list\">
            {% for i,item in benefits %}
            <div class=\"adress\">
                <div class=\"icon\">
                    <img class=\"\" src=\"{{ item.img | media }}\" alt=\"\" data-animation=\"fadeIn\">
                </div>
                <div class=\"content\">
                    <div class=\"tit\" data-animation=\"fadeIn\">
                        {{ item.title }}
                    </div>
                    <div class=\"greyBar\" data-animation=\"fadeIn\"></div>
                    <div class=\"description\" data-animation=\"fadeIn\">
                        {{ item.text }}
                    </div>
                </div>
            </div>
            {% endfor %}
            <div class=\"btn_scroll scroll_btn\" data-animation=\"fadeIn\">
                ZOBACZ OFERTY PRACY
            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"{{  imgText_2_img | media }}\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        {{imgText_2_title}}
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        {{imgText_2_text}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
{% if records | length > 0 %}
<section class=\"simple offers\">
    <div class=\"container\">

        <h3 class=\"title\" data-animation=\"fadeIn\">
            Aktualne Oferty Pracy
        </h3>
        <div class=\"offerInfo_list\">
            {% for i,item in records %}
            <div class=\"offerInfo_el\" data-animation=\"fadeIn\">
                <div class=\"offerInfo\">
                    <div class=\"offerInfoInside\">
                        <h4>{{item.name}}</h4>
                        <p>{{item.section}}</p>
                    </div>
                    <div class=\"locationInfo\">
                        <img src=\"public/img/workOffers/location.svg\"/>
                        <p class=\"locationText\">{{item.location}}</p>
                    </div>
                    <img class=\"dropdown\" data-id=\"{{i}}\" src=\"{{ 'career/arrow_down.svg' | media }}\"/>
                </div>
                <div class=\"offer-content\" data-id=\"{{i}}\">
                    {% if item.description %}
                    <h4 class=\"offerTitle\">Opis oferty</h4>
                    <p class=\"text offerText\">
                        {{ item.description }}
                    </p>
                    {% endif %}
                    {% if item.expectations | length > 0 %}
                    <h4 class=\"offerTitle\">Czego oczekujemy</h4>
                    <ul class=\"offerList\">
                        {% for i,el in item.expectations %}
                        <li><img src=\"{{ 'career/yes.svg' | media }}\"/>
                            <p>{{el.text1}}</p>
                        </li>
                        {% endfor %}
                    </ul>
                    {% endif %}
                    {% if item.offer | length > 0 %}
                    <h4 class=\"offerTitle\">Co oferujemy</h4>
                    <ul class=\"offerList\">
                        {% for i,el in item.offer %}
                        <li><img src=\"{{ 'career/yes.svg' | media }}\"/>
                            <p>{{el.text1}}</p>
                        </li>
                        {% endfor %}
                    </ul>
                    {% endif %}
                    <a class=\"arrow_btn_bg arrow_apply\" href=\"kariera/formularz\">
                        Aplikuj na stanowisko
                    </a>

                </div>
            </div>
            {% endfor %}
        </div>

    </div>
</section>
<section class=\"simpleWithBtn offerWork\">
    <div class=\"container\">
        <h3 class=\"title\">
            {{text_2_title}}
        </h3>
        <p class=\"text\">
            {{text_2_text}}
        </p>
        <a href=\"kariera/formularz\" class=\"arrow_btn_bg\">
            Napisz do nas
        </a>
    </div>
</section>
{% else %}
<section class=\"simpleWithBtn offerWork\">
    <div class=\"container\">
        <h3 class=\"title\">
            Aktualnie brak ofert pracy
        </h3>
        <p class=\"text\">
            Chcesz z nami pracowć? Napisz do nas wyślij nam dwoje CV a by na pewno je rozpatrzymy!
        </p>
        <a href=\"kariera/formularz\" class=\"arrow_btn_bg\">
            Napisz do nas
        </a>
    </div>
</section>
{% endif %}

{% partial 'footer' %}
<script>

    const offerContent = Array.from(document.querySelectorAll('.offer-content'));

    offerContent.forEach(function (el) {
        const elHeight = el.offsetHeight;
        el.setAttribute('data-height', elHeight);
        el.style.height = \"0px\";
    });

    \$(document).ready(function () {

        \$('.scroll_btn').on('click', function () {
            \$('body,html').animate({
                scrollTop: \$('.simple.offers').offset().top
            }, 1000);
        });

        \$('.dropdown').on('click', function () {
            const target = \$(this).attr('data-id');
            const isActive = \$(this).hasClass('active');

            \$('.dropdown').removeClass('active');
            \$('.offer-content').css({'height': '0px'});

            if (!isActive) {
                \$(this).addClass('active');
                const elHeight = \$('.offer-content[data-id=\"' + target + '\"]').attr('data-height');
                console.log('elHeight', elHeight);
                \$('.offer-content[data-id=\"' + target + '\"]').css({height: elHeight + 'px'});


            }
        });
    });
</script>
</body>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/career.htm", "");
    }
}
