<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/layouts/singleProduct.htm */
class __TwigTemplate_a55dc15d631ad74a10f06978421ac38ea0e6bd7256867e125f32afe987286771 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 3
        echo "

";
        // line 5
        $context["record"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "record", [], "any", false, false, false, 5);
        // line 6
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "displayColumn", [], "any", false, false, false, 6);
        // line 7
        $context["notFoundMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "notFoundMessage", [], "any", false, false, false, 7);
        // line 8
        $context["staticData"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails2"] ?? null), "record", [], "any", false, false, false, 8);
        // line 9
        $context["displayColumn2"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails2"] ?? null), "displayColumn", [], "any", false, false, false, 9);
        // line 10
        $context["notFoundMessage2"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails2"] ?? null), "notFoundMessage", [], "any", false, false, false, 10);
        // line 11
        echo "
";
        // line 12
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 12);
        // line 13
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 13);
        // line 14
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 14);
        // line 15
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 15);
        // line 16
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 16);
        // line 17
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 17);
        // line 18
        echo "

<body class=\"singleProduct\">

";
        // line 22
        if (($context["record"] ?? null)) {
            // line 23
            echo "<section class=\"hero\" alt=\"\" data-animation=\"fadeIn\">

    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <a href=\"../produkty\" class=\"btn_back\" data-animation=\"slideFromLeft\">
                produkty
            </a>
            <p class=\"subTitle\" data-animation=\"slideFromLeft\">
                Produkty
            </p>
            <h1 class=\"title\" data-animation=\"slideFromLeft\">
                ";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "name", [], "any", false, false, false, 36), "html", null, true);
            echo "
            </h1>
            <p class=\"description\" data-animation=\"slideFromLeft\">
                ";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "description", [], "any", false, false, false, 39), "html", null, true);
            echo "
            </p>
        </div>
    </div>
    <img class=\"mainImg mainImg_desktop\" src=\"";
            // line 43
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "main_img", [], "any", false, false, false, 43));
            echo "\">
</section>

<div class=\"singleProduct_nav\" data-animation=\"fadeIn\">
    <div class=\"container\">
        <div class=\"row\">
            ";
            // line 49
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "properties", [], "any", false, false, false, 49)) > 0)) {
                // line 50
                echo "            <div class=\"scroll_el\" data-scroll=\"properties\">Właściwości</div>
            ";
            }
            // line 52
            echo "            ";
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "tech_data", [], "any", false, false, false, 52)) > 0)) {
                // line 53
                echo "            <div class=\"scroll_el\" data-scroll=\"data-tech\">Dane techniczne</div>
            ";
            }
            // line 55
            echo "            ";
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "download", [], "any", false, false, false, 55)) > 0)) {
                // line 56
                echo "            <div class=\"scroll_el\" data-scroll=\"materials\">Materiały do pobrania</div>
            ";
            }
            // line 58
            echo "        </div>
    </div>
</div>
";
            // line 61
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "properties", [], "any", false, false, false, 61)) > 0)) {
                // line 62
                echo "<div class=\"singleProduct_properties\" data-scroll-target=\"properties\">
    <div class=\"container\">
        <div class=\"title\" data-animation=\"fadeIn\">
            Właściwości płyty
        </div>
        ";
                // line 67
                echo twig_escape_filter($this->env, ($context["productlist_static"] ?? null), "html", null, true);
                echo "
        <ul class=\"list\" data-animation=\"fadeIn\">
            ";
                // line 69
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "properties", [], "any", false, false, false, 69));
                foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                    // line 70
                    echo "            <li>
                <div class=\"icon\">
                    <img src=\"";
                    // line 72
                    echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "icon", [], "any", false, false, false, 72));
                    echo "\" alt=\"\">
                </div>
                <p>";
                    // line 74
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "text", [], "any", false, false, false, 74), "html", null, true);
                    echo "</p>
            </li>

            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 78
                echo "        </ul>
    </div>
    <div id=\"prop_slider\" class=\"slider\" data-animation=\"slideFromRight\">
        ";
                // line 81
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "slider", [], "any", false, false, false, 81));
                foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                    // line 82
                    echo "
        <div class=\"slide\">
            <div class=\"counter\">
                ";
                    // line 85
                    echo twig_escape_filter($this->env, ($context["i"] + 1), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "slider", [], "any", false, false, false, 85)), "html", null, true);
                    echo "
            </div>
            <img src=\"";
                    // line 87
                    echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "img", [], "any", false, false, false, 87));
                    echo "\" alt=\"\">
            ";
                    // line 88
                    if (($context["i"] == 0)) {
                        // line 89
                        echo "            <div class=\"tooltip\" style=\"top: 60%; left: 40%;\">
                <div class=\"tooltip_hover\">
                    <span></span>
                    <span></span>
                </div>
                <div class=\"tooltip_content\">
                    <p class=\"tooltip_content_title\">
                        Cecha produktu
                    </p>
                    <p class=\"tooltip_content_text\">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                    </p>
                </div>
            </div>
            <div class=\"tooltip\" style=\"top: 40%; left: 70%;\">
                <div class=\"tooltip_hover\">
                    <span></span>
                    <span></span>
                </div>
                <div class=\"tooltip_content\">
                    <p class=\"tooltip_content_title\">
                        Cecha produktu
                    </p>
                    <p class=\"tooltip_content_text\">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                    </p>
                </div>
            </div>
            ";
                    }
                    // line 118
                    echo "        </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 120
                echo "    </div>
</div>
";
            }
            // line 123
            echo "
<div class=\"technical_data\" data-scroll-target=\"data-tech\">
    <div class=\"container\">
        <div class=\"title\" data-animation=\"fadeIn\">
            Dane techniczne
        </div>
        <div class=\"data_list\" data-animation=\"fadeIn\">
            ";
            // line 130
            echo twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "tech_data", [], "any", false, false, false, 130);
            echo "


        </div>
    </div>
</div>

<section class=\"aboutProduct\">
    <img class=\"shiftedTile_bg\" src=\"";
            // line 138
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["records"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "about_product_img", [], "any", false, false, false, 138));
            echo "\" alt=\"\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"img_col\">
                <div class=\"box\">

                </div>
            </div>
            <div class=\"tile_col\" data-animation=\"slideFromRight\">

                <p class=\"text\">
                    Dzięki szerokiej gamie dekorów oraz struktur powierzchni, płyty laminowane są podstawowym materiałem
                    do produkcji mebli oraz elementów wykończenia wnętrz.
                </p>

            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\" data-animation=\"slideFromLeft\">
                <img src=\"";
            // line 161
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["records"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[0] ?? null) : null), "img_with_text_img", [], "any", false, false, false, 161));
            echo "\" alt=\"\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        Zabezpieczenie UV
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        Jako odpowiednio zabezpieczony produkt, płyty laminowane charakteryzują się wysoką odpornością
                        na działania promieni UV, zabrudzenia i środki chemiczne. Ponadto nie gromadzą ładunków
                        elektrostatycznych na swojej powierzchni, przez co nie przyciągają kurzu.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

";
            // line 179
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "download", [], "any", false, false, false, 179)) > 0)) {
                // line 180
                echo "<section class=\"productDownloadMeterials\" data-scroll-target=\"materials\">
    <div class=\"container\">
        <div class=\"title\" data-animation=\"fadeIn\">
            Materiały do pobrania
        </div>


        <div class=\"productDownloadMeterials_list\">
            ";
                // line 188
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "download", [], "any", false, false, false, 188));
                foreach ($context['_seq'] as $context["i"] => $context["item"]) {
                    // line 189
                    echo "            <div class=\"productDownloadMeterials_el\"
                 data-animation=\"fadeIn\">
                <div class=\"left_content\">
                    <p class=\"productDownloadMeterials_title\">
                        ";
                    // line 193
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "text", [], "any", false, false, false, 193), "html", null, true);
                    echo "
                    </p>
                </div>
                <div class=\"right_content\">
                    <div class=\"type_icon\">PDF</div>
                    <a href=\"";
                    // line 198
                    echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "file", [], "any", false, false, false, 198));
                    echo "\" target=\"_blank\" class=\"download_arrow\">
                        <span></span><span></span><span></span>
                    </a>
                </div>
            </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 204
                echo "        </div>

    </div>
</section>
";
            }
            // line 209
            echo "<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"fadeIn\">
            Zainteresował Cię ten produkt?
        </h3>
        <p class=\"text\" data-animation=\"slideFromLeft\">
            Kliknij \"Zamów produkt\" i skorzystaj z formularza zamówienia
        </p>
        <a class=\"arrow_btn_bg\" href=\"\" data-animation=\"slideFromRight\">
            Zamów PRODUKT
        </a>
    </div>
</section>


";
            // line 224
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 225
            echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css\"/>
<script type=\"text/javascript\" src=\"//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js\"></script>
<script>
    \$('#prop_slider').slick({
        slidesToShow: 1.5,
        centerMode: true,
        slidesToScroll: 1.5,
        infinite: false,
        prevArrow: '<div class=\"slider_prev slider_nav\"></div>',
        nextArrow: '<div class=\"slider_next slider_nav\"></div>',
        responsive: [{
            breakpoint: 770,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
            }
        }]
    });
    \$(document).ready(function () {
        \$('.tooltip').mouseover(function () {
            console.log(\$(this));
            \$(this).addClass('active');
        });
        \$('.tooltip').mouseleave(function () {
            console.log(\$(this));
            \$(this).removeClass('active');
        });
        \$('.scroll_el').on('click', function () {
            const target = \$(this).attr('data-scroll');
            const \$target = \$('[data-scroll-target=\"' + target + '\"]');
            \$([document.documentElement, document.body]).animate({
                scrollTop: \$target.offset().top
            }, 2000);
        });
    });

</script>
";
        } else {
            // line 264
            echo "404
";
        }
        // line 266
        echo "</body>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/singleProduct.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 266,  431 => 264,  390 => 225,  386 => 224,  369 => 209,  362 => 204,  350 => 198,  342 => 193,  336 => 189,  332 => 188,  322 => 180,  320 => 179,  299 => 161,  273 => 138,  262 => 130,  253 => 123,  248 => 120,  241 => 118,  210 => 89,  208 => 88,  204 => 87,  197 => 85,  192 => 82,  188 => 81,  183 => 78,  173 => 74,  168 => 72,  164 => 70,  160 => 69,  155 => 67,  148 => 62,  146 => 61,  141 => 58,  137 => 56,  134 => 55,  130 => 53,  127 => 52,  123 => 50,  121 => 49,  112 => 43,  105 => 39,  99 => 36,  84 => 23,  82 => 22,  76 => 18,  74 => 17,  72 => 16,  70 => 15,  68 => 14,  66 => 13,  64 => 12,  61 => 11,  59 => 10,  57 => 9,  55 => 8,  53 => 7,  51 => 6,  49 => 5,  45 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% partial 'header' %}
{% partial 'navigation' %}


{% set record = builderDetails.record %}
{% set displayColumn = builderDetails.displayColumn %}
{% set notFoundMessage = builderDetails.notFoundMessage %}
{% set staticData = builderDetails2.record %}
{% set displayColumn2 = builderDetails2.displayColumn %}
{% set notFoundMessage2 = builderDetails2.notFoundMessage %}

{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}


<body class=\"singleProduct\">

{% if record %}
<section class=\"hero\" alt=\"\" data-animation=\"fadeIn\">

    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <a href=\"../produkty\" class=\"btn_back\" data-animation=\"slideFromLeft\">
                produkty
            </a>
            <p class=\"subTitle\" data-animation=\"slideFromLeft\">
                Produkty
            </p>
            <h1 class=\"title\" data-animation=\"slideFromLeft\">
                {{record.name}}
            </h1>
            <p class=\"description\" data-animation=\"slideFromLeft\">
                {{record.description}}
            </p>
        </div>
    </div>
    <img class=\"mainImg mainImg_desktop\" src=\"{{ record.main_img | media }}\">
</section>

<div class=\"singleProduct_nav\" data-animation=\"fadeIn\">
    <div class=\"container\">
        <div class=\"row\">
            {% if record.properties|length > 0 %}
            <div class=\"scroll_el\" data-scroll=\"properties\">Właściwości</div>
            {% endif %}
            {% if record.tech_data|length > 0 %}
            <div class=\"scroll_el\" data-scroll=\"data-tech\">Dane techniczne</div>
            {% endif %}
            {% if record.download|length > 0 %}
            <div class=\"scroll_el\" data-scroll=\"materials\">Materiały do pobrania</div>
            {% endif %}
        </div>
    </div>
</div>
{% if record.properties|length > 0 %}
<div class=\"singleProduct_properties\" data-scroll-target=\"properties\">
    <div class=\"container\">
        <div class=\"title\" data-animation=\"fadeIn\">
            Właściwości płyty
        </div>
        {{productlist_static}}
        <ul class=\"list\" data-animation=\"fadeIn\">
            {% for i,item in record.properties %}
            <li>
                <div class=\"icon\">
                    <img src=\"{{ item.icon | media }}\" alt=\"\">
                </div>
                <p>{{ item.text }}</p>
            </li>

            {% endfor %}
        </ul>
    </div>
    <div id=\"prop_slider\" class=\"slider\" data-animation=\"slideFromRight\">
        {% for i,item in record.slider %}

        <div class=\"slide\">
            <div class=\"counter\">
                {{i + 1}}/{{record.slider|length}}
            </div>
            <img src=\"{{ item.img | media }}\" alt=\"\">
            {% if i == 0 %}
            <div class=\"tooltip\" style=\"top: 60%; left: 40%;\">
                <div class=\"tooltip_hover\">
                    <span></span>
                    <span></span>
                </div>
                <div class=\"tooltip_content\">
                    <p class=\"tooltip_content_title\">
                        Cecha produktu
                    </p>
                    <p class=\"tooltip_content_text\">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                    </p>
                </div>
            </div>
            <div class=\"tooltip\" style=\"top: 40%; left: 70%;\">
                <div class=\"tooltip_hover\">
                    <span></span>
                    <span></span>
                </div>
                <div class=\"tooltip_content\">
                    <p class=\"tooltip_content_title\">
                        Cecha produktu
                    </p>
                    <p class=\"tooltip_content_text\">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                    </p>
                </div>
            </div>
            {% endif %}
        </div>
        {% endfor %}
    </div>
</div>
{% endif %}

<div class=\"technical_data\" data-scroll-target=\"data-tech\">
    <div class=\"container\">
        <div class=\"title\" data-animation=\"fadeIn\">
            Dane techniczne
        </div>
        <div class=\"data_list\" data-animation=\"fadeIn\">
            {{ record.tech_data | raw }}


        </div>
    </div>
</div>

<section class=\"aboutProduct\">
    <img class=\"shiftedTile_bg\" src=\"{{ records[0].about_product_img | media }}\" alt=\"\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"img_col\">
                <div class=\"box\">

                </div>
            </div>
            <div class=\"tile_col\" data-animation=\"slideFromRight\">

                <p class=\"text\">
                    Dzięki szerokiej gamie dekorów oraz struktur powierzchni, płyty laminowane są podstawowym materiałem
                    do produkcji mebli oraz elementów wykończenia wnętrz.
                </p>

            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\" data-animation=\"slideFromLeft\">
                <img src=\"{{ records[0].img_with_text_img | media }}\" alt=\"\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        Zabezpieczenie UV
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        Jako odpowiednio zabezpieczony produkt, płyty laminowane charakteryzują się wysoką odpornością
                        na działania promieni UV, zabrudzenia i środki chemiczne. Ponadto nie gromadzą ładunków
                        elektrostatycznych na swojej powierzchni, przez co nie przyciągają kurzu.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

{% if record.download|length > 0 %}
<section class=\"productDownloadMeterials\" data-scroll-target=\"materials\">
    <div class=\"container\">
        <div class=\"title\" data-animation=\"fadeIn\">
            Materiały do pobrania
        </div>


        <div class=\"productDownloadMeterials_list\">
            {% for i,item in record.download %}
            <div class=\"productDownloadMeterials_el\"
                 data-animation=\"fadeIn\">
                <div class=\"left_content\">
                    <p class=\"productDownloadMeterials_title\">
                        {{item.text}}
                    </p>
                </div>
                <div class=\"right_content\">
                    <div class=\"type_icon\">PDF</div>
                    <a href=\"{{ item.file | media }}\" target=\"_blank\" class=\"download_arrow\">
                        <span></span><span></span><span></span>
                    </a>
                </div>
            </div>
            {% endfor %}
        </div>

    </div>
</section>
{% endif %}
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"fadeIn\">
            Zainteresował Cię ten produkt?
        </h3>
        <p class=\"text\" data-animation=\"slideFromLeft\">
            Kliknij \"Zamów produkt\" i skorzystaj z formularza zamówienia
        </p>
        <a class=\"arrow_btn_bg\" href=\"\" data-animation=\"slideFromRight\">
            Zamów PRODUKT
        </a>
    </div>
</section>


{% partial 'footer' %}
<link rel=\"stylesheet\" type=\"text/css\" href=\"//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css\"/>
<script type=\"text/javascript\" src=\"//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js\"></script>
<script>
    \$('#prop_slider').slick({
        slidesToShow: 1.5,
        centerMode: true,
        slidesToScroll: 1.5,
        infinite: false,
        prevArrow: '<div class=\"slider_prev slider_nav\"></div>',
        nextArrow: '<div class=\"slider_next slider_nav\"></div>',
        responsive: [{
            breakpoint: 770,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
            }
        }]
    });
    \$(document).ready(function () {
        \$('.tooltip').mouseover(function () {
            console.log(\$(this));
            \$(this).addClass('active');
        });
        \$('.tooltip').mouseleave(function () {
            console.log(\$(this));
            \$(this).removeClass('active');
        });
        \$('.scroll_el').on('click', function () {
            const target = \$(this).attr('data-scroll');
            const \$target = \$('[data-scroll-target=\"' + target + '\"]');
            \$([document.documentElement, document.body]).animate({
                scrollTop: \$target.offset().top
            }, 2000);
        });
    });

</script>
{% else %}
404
{% endif %}
</body>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/singleProduct.htm", "");
    }
}
