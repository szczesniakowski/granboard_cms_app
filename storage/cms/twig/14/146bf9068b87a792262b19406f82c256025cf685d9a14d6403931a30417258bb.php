<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/layouts/contact.htm */
class __TwigTemplate_9404f66d1b1218908c76b4ce0907868bd50dfbfbefb976154976514b32d7e6e7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "

















<body class=\"contact\">
";
        // line 20
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 21
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 22
        echo "<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"";
        // line 23
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["hero_img"] ?? null));
        echo "\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"fadeIn\">
                Kontakt
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                ";
        // line 32
        echo twig_escape_filter($this->env, ($context["hero_mainText"] ?? null), "html", null, true);
        echo "
            </h1>
        </div>
    </div>
</section>
<section class=\"contactInfo\">
    <div class=\"container\">
        <div class=\"title\" data-animation=\"fadeIn\">
            ";
        // line 40
        echo twig_escape_filter($this->env, ($context["contactInfo_title"] ?? null), "html", null, true);
        echo "
        </div>
        <div class=\"list\">
            <div class=\"adress\">
                <div class=\"icon\">
                    <img class=\"\" src=\"";
        // line 45
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["contactInfo_1_icon"] ?? null));
        echo "\" alt=\"\" data-animation=\"fadeIn\">
                </div>
                <div class=\"content\">
                    <p class=\"subTit\" data-animation=\"fadeIn\">
                        ";
        // line 49
        echo twig_escape_filter($this->env, ($context["contactInfo_1_title"] ?? null), "html", null, true);
        echo "
                    </p>
                    <div class=\"tit\" data-animation=\"fadeIn\">
                        ";
        // line 52
        echo twig_escape_filter($this->env, ($context["contactInfo_1_mainText"] ?? null), "html", null, true);
        echo "
                    </div>
                    <div class=\"description\" data-animation=\"fadeIn\">
                        ";
        // line 55
        echo twig_escape_filter($this->env, ($context["contactInfo_1_text"] ?? null), "html", null, true);
        echo "
                    </div>
                    <div class=\"btn_scroll\" data-animation=\"fadeIn\">
                        ";
        // line 58
        echo twig_escape_filter($this->env, ($context["contactInfo_1_btnText"] ?? null), "html", null, true);
        echo "
                    </div>
                </div>
            </div>
            <div class=\"teleadress\">
                <div class=\"icon\">
                    <img src=\"";
        // line 64
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["contactInfo_1_icon"] ?? null));
        echo "\" alt=\"\" data-animation=\"fadeIn\">
                </div>
                <div class=\"content\">
                    <p class=\"subTit\" data-animation=\"fadeIn\">
                        ";
        // line 68
        echo twig_escape_filter($this->env, ($context["contactInfo_2_title"] ?? null), "html", null, true);
        echo "
                    </p>
                    <div class=\"tit\" data-animation=\"fadeIn\">
                        ";
        // line 71
        echo twig_escape_filter($this->env, ($context["contactInfo_2_mainText"] ?? null), "html", null, true);
        echo "
                    </div>
                    <div class=\"description\" data-animation=\"fadeIn\">
                        ";
        // line 74
        echo twig_escape_filter($this->env, ($context["contactInfo_2_text"] ?? null), "html", null, true);
        echo "
                    </div>
                    <div class=\"btn_scroll\" data-animation=\"fadeIn\">
                        ";
        // line 77
        echo twig_escape_filter($this->env, ($context["contactInfo_2_btnText"] ?? null), "html", null, true);
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id=\"map\" data-animation=\"fadeIn\"></section>
<section class=\"netSiteForm\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"fadeIn\">
            Napisz do nas
        </h3>
        <form id=\"form\" action=\"\">
            <div class=\"form_row\">
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"companyName\">Nazwa Firmy</label>
                    <input id=\"companyName\" name=\"companyName\" class=\"formField_el\" type=\"text\">
                </div>
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"email\">E-mail</label>
                    <input id=\"email\" name=\"email\" class=\"formField_el\" type=\"text\">
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"name\">Imię i Nazwisko</label>
                    <input id=\"name\" name=\"name\" class=\"formField_el\" type=\"text\">
                </div>
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"phoneNumber\">Numer telefonu</label>
                    <input id=\"phoneNumber\" name=\"phoneNumber\" class=\"formField_el\" type=\"text\">
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"question\">Cel kontaktu</label>
                    <select class=\"formField_el\" name=\"question\" id=\"question\">
                        <option value=\"\">Wybierz</option>
                        <option value=\"\">Opcja 1</option>
                        <option value=\"\">Opcja 2</option>
                        <option value=\"\">Opcja 3</option>
                    </select>
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"message\">Treść wiadomości</label>
                    <textarea id=\"message\" name=\"message\" class=\"formField_el\" name=\"\" cols=\"30\" rows=\"50\"></textarea>
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_agree\">
                    <label class=\"agreeInput\" for=\"agree\" data-animation=\"fadeIn\"></label>
                    <label class=\"formField_label\" for=\"agree\" data-animation=\"fadeIn\">
                        Wyrażam zgodę na wykorzystanie moich danych osobowych przez firmę GranBoard Sp. z o.o.
                        (Administrator danych) w celu udzielenia mi dodatkowych informacji handlowych oraz informacji
                        związanych z zakresem działalności firmy. Podanie danych jest dobrowolne umożliwia uzyskanie w/w
                        informacji. Więcej informacji dotyczących przetwarzania danych znajdą Państwo w naszej polityce
                        prywatności.
                    </label>
                    <input type=\"checkbox\" id=\"agree\" name=\"agree\" hidden>
                </div>
            </div>
            <button class=\"submitBTN\" data-animation=\"fadeIn\">Wyślij</button>
        </form>
    </div>
</section>
";
        // line 145
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 146
        echo "<script>

    \$('form').on('submit', function () {
        let isok = false;
        const fields = \$('.formField_el[data-required=\"true\"]');
        \$.each(fields, function (index, item) {
            const el = \$(item);
            console.log(el);
        });
        return isok;
    });

    const validation = function (field) {
        const validator = field.getAttribute('data-validator');
        switch (validator) {
            case 'text':
                break;
            case 'email':
                break;
            case 'text':
                break;
        }
    }
</script>
<script>
    function initMap() {
        var uluru = {lat: 51.512423, lng: 17.647913};

        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(51.512423, 17.647913),
            styles: [
                {
                    \"featureType\": \"all\",
                    \"elementType\": \"geometry.fill\",
                    \"stylers\": [
                        {
                            \"lightness\": \"74\"
                        },
                        {
                            \"saturation\": \"-77\"
                        },
                        {
                            \"color\": \"#151f2a\"
                        },
                        {
                            \"gamma\": \"3.24\"
                        }
                    ]
                },
                {
                    \"featureType\": \"all\",
                    \"elementType\": \"geometry.stroke\",
                    \"stylers\": [
                        {
                            \"color\": \"#151f2a\"
                        },
                        {
                            \"lightness\": \"-37\"
                        }
                    ]
                },
                {
                    \"featureType\": \"administrative\",
                    \"elementType\": \"labels.text.fill\",
                    \"stylers\": [
                        {
                            \"color\": \"#444444\"
                        }
                    ]
                },
                {
                    \"featureType\": \"landscape\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"color\": \"#f2f2f2\"
                        }
                    ]
                },
                {
                    \"featureType\": \"poi\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"visibility\": \"off\"
                        }
                    ]
                },
                {
                    \"featureType\": \"road\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"saturation\": -100
                        },
                        {
                            \"lightness\": 45
                        }
                    ]
                },
                {
                    \"featureType\": \"road.highway\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"visibility\": \"simplified\"
                        }
                    ]
                },
                {
                    \"featureType\": \"road.arterial\",
                    \"elementType\": \"labels.icon\",
                    \"stylers\": [
                        {
                            \"visibility\": \"off\"
                        }
                    ]
                },
                {
                    \"featureType\": \"transit\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"visibility\": \"off\"
                        }
                    ]
                },
                {
                    \"featureType\": \"water\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"color\": \"#46bcec\"
                        },
                        {
                            \"visibility\": \"on\"
                        }
                    ]
                }
            ],
            mapTypeId: 'terrain'
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var start_point = new google.maps.Marker({
            position: uluru,
            map: map,
            icon: \"public/img/contact/marker.svg\",
            title: 'Start'
        });
    }


</script>
<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyChaTzQxYCHfMP1j-hWCwecDX9ByHxX3kU&callback=initMap\"></script>


</body>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 146,  230 => 145,  159 => 77,  153 => 74,  147 => 71,  141 => 68,  134 => 64,  125 => 58,  119 => 55,  113 => 52,  107 => 49,  100 => 45,  92 => 40,  81 => 32,  69 => 23,  66 => 22,  62 => 21,  58 => 20,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("

















<body class=\"contact\">
{% partial 'header' %}
{% partial 'navigation' %}
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"{{ hero_img | media }}\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"fadeIn\">
                Kontakt
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                {{hero_mainText}}
            </h1>
        </div>
    </div>
</section>
<section class=\"contactInfo\">
    <div class=\"container\">
        <div class=\"title\" data-animation=\"fadeIn\">
            {{contactInfo_title}}
        </div>
        <div class=\"list\">
            <div class=\"adress\">
                <div class=\"icon\">
                    <img class=\"\" src=\"{{ contactInfo_1_icon | media }}\" alt=\"\" data-animation=\"fadeIn\">
                </div>
                <div class=\"content\">
                    <p class=\"subTit\" data-animation=\"fadeIn\">
                        {{contactInfo_1_title}}
                    </p>
                    <div class=\"tit\" data-animation=\"fadeIn\">
                        {{contactInfo_1_mainText}}
                    </div>
                    <div class=\"description\" data-animation=\"fadeIn\">
                        {{contactInfo_1_text}}
                    </div>
                    <div class=\"btn_scroll\" data-animation=\"fadeIn\">
                        {{contactInfo_1_btnText}}
                    </div>
                </div>
            </div>
            <div class=\"teleadress\">
                <div class=\"icon\">
                    <img src=\"{{ contactInfo_1_icon | media }}\" alt=\"\" data-animation=\"fadeIn\">
                </div>
                <div class=\"content\">
                    <p class=\"subTit\" data-animation=\"fadeIn\">
                        {{contactInfo_2_title}}
                    </p>
                    <div class=\"tit\" data-animation=\"fadeIn\">
                        {{contactInfo_2_mainText}}
                    </div>
                    <div class=\"description\" data-animation=\"fadeIn\">
                        {{contactInfo_2_text}}
                    </div>
                    <div class=\"btn_scroll\" data-animation=\"fadeIn\">
                        {{contactInfo_2_btnText}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id=\"map\" data-animation=\"fadeIn\"></section>
<section class=\"netSiteForm\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"fadeIn\">
            Napisz do nas
        </h3>
        <form id=\"form\" action=\"\">
            <div class=\"form_row\">
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"companyName\">Nazwa Firmy</label>
                    <input id=\"companyName\" name=\"companyName\" class=\"formField_el\" type=\"text\">
                </div>
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"email\">E-mail</label>
                    <input id=\"email\" name=\"email\" class=\"formField_el\" type=\"text\">
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"name\">Imię i Nazwisko</label>
                    <input id=\"name\" name=\"name\" class=\"formField_el\" type=\"text\">
                </div>
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"phoneNumber\">Numer telefonu</label>
                    <input id=\"phoneNumber\" name=\"phoneNumber\" class=\"formField_el\" type=\"text\">
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"question\">Cel kontaktu</label>
                    <select class=\"formField_el\" name=\"question\" id=\"question\">
                        <option value=\"\">Wybierz</option>
                        <option value=\"\">Opcja 1</option>
                        <option value=\"\">Opcja 2</option>
                        <option value=\"\">Opcja 3</option>
                    </select>
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"message\">Treść wiadomości</label>
                    <textarea id=\"message\" name=\"message\" class=\"formField_el\" name=\"\" cols=\"30\" rows=\"50\"></textarea>
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_agree\">
                    <label class=\"agreeInput\" for=\"agree\" data-animation=\"fadeIn\"></label>
                    <label class=\"formField_label\" for=\"agree\" data-animation=\"fadeIn\">
                        Wyrażam zgodę na wykorzystanie moich danych osobowych przez firmę GranBoard Sp. z o.o.
                        (Administrator danych) w celu udzielenia mi dodatkowych informacji handlowych oraz informacji
                        związanych z zakresem działalności firmy. Podanie danych jest dobrowolne umożliwia uzyskanie w/w
                        informacji. Więcej informacji dotyczących przetwarzania danych znajdą Państwo w naszej polityce
                        prywatności.
                    </label>
                    <input type=\"checkbox\" id=\"agree\" name=\"agree\" hidden>
                </div>
            </div>
            <button class=\"submitBTN\" data-animation=\"fadeIn\">Wyślij</button>
        </form>
    </div>
</section>
{% partial 'footer' %}
<script>

    \$('form').on('submit', function () {
        let isok = false;
        const fields = \$('.formField_el[data-required=\"true\"]');
        \$.each(fields, function (index, item) {
            const el = \$(item);
            console.log(el);
        });
        return isok;
    });

    const validation = function (field) {
        const validator = field.getAttribute('data-validator');
        switch (validator) {
            case 'text':
                break;
            case 'email':
                break;
            case 'text':
                break;
        }
    }
</script>
<script>
    function initMap() {
        var uluru = {lat: 51.512423, lng: 17.647913};

        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(51.512423, 17.647913),
            styles: [
                {
                    \"featureType\": \"all\",
                    \"elementType\": \"geometry.fill\",
                    \"stylers\": [
                        {
                            \"lightness\": \"74\"
                        },
                        {
                            \"saturation\": \"-77\"
                        },
                        {
                            \"color\": \"#151f2a\"
                        },
                        {
                            \"gamma\": \"3.24\"
                        }
                    ]
                },
                {
                    \"featureType\": \"all\",
                    \"elementType\": \"geometry.stroke\",
                    \"stylers\": [
                        {
                            \"color\": \"#151f2a\"
                        },
                        {
                            \"lightness\": \"-37\"
                        }
                    ]
                },
                {
                    \"featureType\": \"administrative\",
                    \"elementType\": \"labels.text.fill\",
                    \"stylers\": [
                        {
                            \"color\": \"#444444\"
                        }
                    ]
                },
                {
                    \"featureType\": \"landscape\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"color\": \"#f2f2f2\"
                        }
                    ]
                },
                {
                    \"featureType\": \"poi\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"visibility\": \"off\"
                        }
                    ]
                },
                {
                    \"featureType\": \"road\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"saturation\": -100
                        },
                        {
                            \"lightness\": 45
                        }
                    ]
                },
                {
                    \"featureType\": \"road.highway\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"visibility\": \"simplified\"
                        }
                    ]
                },
                {
                    \"featureType\": \"road.arterial\",
                    \"elementType\": \"labels.icon\",
                    \"stylers\": [
                        {
                            \"visibility\": \"off\"
                        }
                    ]
                },
                {
                    \"featureType\": \"transit\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"visibility\": \"off\"
                        }
                    ]
                },
                {
                    \"featureType\": \"water\",
                    \"elementType\": \"all\",
                    \"stylers\": [
                        {
                            \"color\": \"#46bcec\"
                        },
                        {
                            \"visibility\": \"on\"
                        }
                    ]
                }
            ],
            mapTypeId: 'terrain'
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var start_point = new google.maps.Marker({
            position: uluru,
            map: map,
            icon: \"public/img/contact/marker.svg\",
            title: 'Start'
        });
    }


</script>
<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyChaTzQxYCHfMP1j-hWCwecDX9ByHxX3kU&callback=initMap\"></script>


</body>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/contact.htm", "");
    }
}
