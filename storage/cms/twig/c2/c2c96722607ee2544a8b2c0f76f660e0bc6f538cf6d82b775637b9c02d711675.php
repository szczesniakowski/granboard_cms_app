<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/layouts/products.htm */
class __TwigTemplate_04cff5e53cd84fffbac5c5252ea4ab6ce0dea10a971d122ba3906fa4d5df60eb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "





";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["extraData"] ?? null), "products", [], "any", false, false, false, 7));
        foreach ($context['_seq'] as $context["_key"] => $context["fields"]) {
            // line 8
            echo "


";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
";
        // line 13
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 14
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 15
        echo "
";
        // line 16
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 16);
        // line 17
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 17);
        // line 18
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 18);
        // line 19
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 19);
        // line 20
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 20);
        // line 21
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 21);
        // line 22
        echo "
<body class=\"productPage\">
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"";
        // line 25
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["hero_img"] ?? null));
        echo "\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"slideFromLeft\">
                Produkty
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                ";
        // line 34
        echo twig_escape_filter($this->env, ($context["hero_mainText"] ?? null), "html", null, true);
        echo "
            </h1>
        </div>
    </div>
</section>
<section class=\"product_view\" data-animation=\"slideFromLeft\">
    <div class=\"container\">
        <p class=\"title\">zmień widok listy</p>
        <div class=\"product_view_list\">
            <div class=\"product_view_list_item \" data-view=\"tiles\">
                <span></span><span></span><span></span><span></span>
            </div>
            <div class=\"product_view_list_item active\" data-view=\"list\">
                <span></span><span></span><span></span><span></span>
            </div>
        </div>
    </div>
</section>
<section class=\"product_list\" data-view=\"list\">
    ";
        // line 53
        $context["index"] = 0;
        // line 54
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 55
            echo "    ";
            if (twig_get_attribute($this->env, $this->source, $context["item"], "show", [], "any", false, false, false, 55)) {
                // line 56
                echo "    <div class=\"product_item ";
                if (((($context["index"] ?? null) % 2) != 0)) {
                    echo " reverse ";
                }
                echo "\">
        <img class=\"product_item_img\" ";
                // line 57
                if (((($context["index"] ?? null) % 2) != 0)) {
                    echo " data-animation-original=\"slideFromLeft\"
             data-animation=\"slideFromLeft\" ";
                } else {
                    // line 58
                    echo " data-animation-original=\"slideFromRight\"
             data-animation=\"slideFromRight\" ";
                }
                // line 59
                echo " src=\"";
                echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "main_img", [], "any", false, false, false, 59));
                echo "\" alt=\"\">
        <div class=\"product_item_content\">
            <div class=\"content\">
                <h4 class=\"product_item_name\" ";
                // line 62
                if (((($context["index"] ?? null) % 2) != 0)) {
                    echo " data-animation-original=\"slideFromRight\"
                    data-animation=\"slideFromRight\" ";
                } else {
                    // line 63
                    echo " data-animation-original=\"slideFromLeft\"
                    data-animation=\"slideFromLeft\" ";
                }
                // line 64
                echo ">
                    ";
                // line 65
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 65), "html", null, true);
                echo "
                </h4>
                <p class=\"product_item_description\" ";
                // line 67
                if (((($context["index"] ?? null) % 2) != 0)) {
                    echo " data-animation-original=\"slideFromRight\"
                   data-animation=\"slideFromRight\" ";
                } else {
                    // line 68
                    echo " data-animation-original=\"slideFromLeft\"
                   data-animation=\"slideFromLeft\" ";
                }
                // line 69
                echo ">
                    ";
                // line 70
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 70), "html", null, true);
                echo "
                </p>
                <a href=\"";
                // line 72
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["item"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 72)]);
                echo "\" ";
                // line 73
                if (((($context["index"] ?? null) % 2) != 0)) {
                    echo " data-animation-original=\"slideFromRight\" data-animation=\"slideFromRight\" ";
                } else {
                    // line 74
                    echo "                   data-animation-original=\"slideFromLeft\" data-animation=\"slideFromLeft\" ";
                }
                // line 75
                echo "                   class=\"product_item_link\">zobacz produkt</a>
            </div>
        </div>
    </div>
    ";
                // line 79
                $context["index"] = (($context["index"] ?? null) + 1);
                // line 80
                echo "    ";
            }
            // line 81
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "</section>
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\">
            ";
        // line 86
        echo twig_escape_filter($this->env, ($context["btnWithText_title"] ?? null), "html", null, true);
        echo "
        </h3>
        <p class=\"text\">
            ";
        // line 89
        echo twig_escape_filter($this->env, ($context["btnWithText_description"] ?? null), "html", null, true);
        echo "
        </p>
        <a class=\"arrow_btn_bg\" href=\"\">
            SKONTAKTUJ SIĘ Z NAMI
        </a>
    </div>
</section>

";
        // line 97
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 98
        echo "
<script>
    \$(document).ready(function () {
        const listView = \$('.product_view_list_item');
        const dataAnimation = \$('[data-animation]');

        \$('.product_view_list_item').on('click', function (e) {
            const target = \$(this).attr('data-view');
            \$('.product_view_list_item').removeClass('active');


            let winTop = \$(window).scrollTop();
            let winHeight = \$(window).height();
            let winBottom = winTop + winHeight;
            let lastScroll = winTop;
            let direction = 'firstView';

            check_isInView(winTop, winHeight, winBottom, lastScroll, direction);


            switch (target) {
                case 'tiles':
                    \$.each(dataAnimation, function (index, el) {
                        el = \$(el);
                        el.attr('data-animation', 'fadeIn');
                    });
                    break;
                case 'list':
                    const originalData = el.attr('[data-aniamtion-original]');
                    el.attr('data-animation', originalData);
                    break;
            }
            \$('.product_view_list_item[data-view=\"' + target + '\"]').addClass('active');

            \$('.product_list').attr('data-view', target);

        });
    });

</script>
</body>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/products.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 98,  240 => 97,  229 => 89,  223 => 86,  217 => 82,  211 => 81,  208 => 80,  206 => 79,  200 => 75,  197 => 74,  193 => 73,  190 => 72,  185 => 70,  182 => 69,  178 => 68,  173 => 67,  168 => 65,  165 => 64,  161 => 63,  156 => 62,  149 => 59,  145 => 58,  140 => 57,  133 => 56,  130 => 55,  125 => 54,  123 => 53,  101 => 34,  89 => 25,  84 => 22,  82 => 21,  80 => 20,  78 => 19,  76 => 18,  74 => 17,  72 => 16,  69 => 15,  65 => 14,  61 => 13,  58 => 12,  49 => 8,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("





{% for fields in extraData.products %}



{% endfor %}

{% partial 'header' %}
{% partial 'navigation' %}

{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

<body class=\"productPage\">
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"{{ hero_img | media }}\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"slideFromLeft\">
                Produkty
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                {{hero_mainText}}
            </h1>
        </div>
    </div>
</section>
<section class=\"product_view\" data-animation=\"slideFromLeft\">
    <div class=\"container\">
        <p class=\"title\">zmień widok listy</p>
        <div class=\"product_view_list\">
            <div class=\"product_view_list_item \" data-view=\"tiles\">
                <span></span><span></span><span></span><span></span>
            </div>
            <div class=\"product_view_list_item active\" data-view=\"list\">
                <span></span><span></span><span></span><span></span>
            </div>
        </div>
    </div>
</section>
<section class=\"product_list\" data-view=\"list\">
    {% set index = 0 %}
    {% for i,item in records %}
    {% if(item.show) %}
    <div class=\"product_item {% if(index%2!=0) %} reverse {% endif %}\">
        <img class=\"product_item_img\" {% if(index%2!=0) %} data-animation-original=\"slideFromLeft\"
             data-animation=\"slideFromLeft\" {% else %} data-animation-original=\"slideFromRight\"
             data-animation=\"slideFromRight\" {% endif %} src=\"{{ item.main_img | media}}\" alt=\"\">
        <div class=\"product_item_content\">
            <div class=\"content\">
                <h4 class=\"product_item_name\" {% if(index%2!=0) %} data-animation-original=\"slideFromRight\"
                    data-animation=\"slideFromRight\" {% else %} data-animation-original=\"slideFromLeft\"
                    data-animation=\"slideFromLeft\" {% endif %}>
                    {{ item.name }}
                </h4>
                <p class=\"product_item_description\" {% if(index%2!=0) %} data-animation-original=\"slideFromRight\"
                   data-animation=\"slideFromRight\" {% else %} data-animation-original=\"slideFromLeft\"
                   data-animation=\"slideFromLeft\" {% endif %}>
                    {{ item.description }}
                </p>
                <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(item, detailsKeyColumn) }) }}\" {%
                   if(index%2!=0) %} data-animation-original=\"slideFromRight\" data-animation=\"slideFromRight\" {% else %}
                   data-animation-original=\"slideFromLeft\" data-animation=\"slideFromLeft\" {% endif %}
                   class=\"product_item_link\">zobacz produkt</a>
            </div>
        </div>
    </div>
    {% set index = index + 1 %}
    {% endif %}
    {% endfor %}
</section>
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\">
            {{btnWithText_title}}
        </h3>
        <p class=\"text\">
            {{btnWithText_description}}
        </p>
        <a class=\"arrow_btn_bg\" href=\"\">
            SKONTAKTUJ SIĘ Z NAMI
        </a>
    </div>
</section>

{% partial 'footer' %}

<script>
    \$(document).ready(function () {
        const listView = \$('.product_view_list_item');
        const dataAnimation = \$('[data-animation]');

        \$('.product_view_list_item').on('click', function (e) {
            const target = \$(this).attr('data-view');
            \$('.product_view_list_item').removeClass('active');


            let winTop = \$(window).scrollTop();
            let winHeight = \$(window).height();
            let winBottom = winTop + winHeight;
            let lastScroll = winTop;
            let direction = 'firstView';

            check_isInView(winTop, winHeight, winBottom, lastScroll, direction);


            switch (target) {
                case 'tiles':
                    \$.each(dataAnimation, function (index, el) {
                        el = \$(el);
                        el.attr('data-animation', 'fadeIn');
                    });
                    break;
                case 'list':
                    const originalData = el.attr('[data-aniamtion-original]');
                    el.attr('data-animation', originalData);
                    break;
            }
            \$('.product_view_list_item[data-view=\"' + target + '\"]').addClass('active');

            \$('.product_list').attr('data-view', target);

        });
    });

</script>
</body>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/products.htm", "");
    }
}
