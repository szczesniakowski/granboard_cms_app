<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/layouts/career-form.htm */
class __TwigTemplate_eeb068f15e71a3a76a32fb2c2351d041ce9265024a90fd973392ada7770d5dc7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<body class=\"careerForm\">
";
        // line 2
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 3
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 4
        echo "
<form class=\"netSiteForm\">
    <div class=\"container\">
        <a href=\"../kariera\" class=\"btn_back\" data-animation=\"slideFromLeft\">
            kariera
        </a>
        <h3 class=\"title\" data-animation=\"fadeIn\">
            Wyślij aplikacje
        </h3>
        
        <form id=\"form\" action=\"\">
            <div class=\"form_row\">
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"name\">Imię i Nazwisko</label>
                    <input id=\"name\" name=\"name\" class=\"formField_el\" type=\"text\" data-required=\"true\">
                </div>
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"email\">E-mail</label>
                    <input id=\"email\" name=\"email\" class=\"formField_el\" type=\"text\" data-required=\"true\">
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"phoneNumber\">Numer telefonu</label>
                    <input id=\"phoneNumber\" name=\"phoneNumber\" class=\"formField_el\" type=\"text\" data-required=\"true\">
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"jobTitle\">Nazwa stanowiska</label>
                    <select id=\"jobTitle\" name=\"jobTitle\" class=\"formField_el\" type=\"text\" data-required=\"true\">
                        <option value=\"none\">Wybierz</option>
                        <option value=\"CNC\">Operator CNC</option>
                    </select>
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full formField_file\" data-animation=\"fadeIn\">
                    <div class=\"formField_file_row\">
                        <label class=\"formField_label\" for=\"file\">CV</label>
                        <label for=\"file\" class=\"addAttach\">
                            <p>Dodaj załącznik</p>
                            <img src=\"";
        // line 46
        echo $this->extensions['System\Twig\Extension']->mediaFilter("career/icon-attachment.svg");
        echo "\" alt=\"\">
                        </label>
                        <input id=\"file\" name=\"file\" type=\"file\" class=\"formField_el hiddenInput\" data-required=\"true\">
                    </div>
                    <div class=\"formField_file_row\">
                        <p class=\"fileName\">Jan_Kowalski_CV.pdf</p>
                    </div>
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_agree\">
                    <label class=\"agreeInput\" for=\"agree\" data-animation=\"fadeIn\"></label>
                    <label class=\"formField_label\" for=\"agree\" data-animation=\"fadeIn\">
                        Wyrażam zgodę na wykorzystanie moich danych osobowych przez firmę GranBoard Sp. z o.o.
                        (Administrator danych) w celu udzielenia mi dodatkowych informacji handlowych oraz informacji
                        związanych z zakresem działalności firmy. Podanie danych jest dobrowolne umożliwia uzyskanie w/w
                        informacji. Więcej informacji dotyczących przetwarzania danych znajdą Państwo w naszej polityce
                        prywatności.
                    </label>
                    <input type=\"checkbox\" id=\"agree\" name=\"agree\" hidden data-required=\"true\">
                </div>
            </div>
            <button class=\"submitBTN\" data-animation=\"fadeIn\">Wyślij</button>
        </form>
    </div>
</form>
";
        // line 72
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 73
        echo "<script>
    \$('form').on('submit', function () {
        let isok = false;
        const fields = \$('.formField_el[data-required=\"true\"]');
        \$.each(fields, function (index, item) {
            const el = \$(item);
            console.log(el);
        });
        return isok;
    });

    const validation = function (field) {
        const validator = field.attr('data-validator');
        switch (validator) {
            case 'text':
                break;
            case 'email':
                break;
            case 'text':
                break;
        }
    }
</script>
</body>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/career-form.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 73,  121 => 72,  92 => 46,  48 => 4,  44 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<body class=\"careerForm\">
{% partial 'header' %}
{% partial 'navigation' %}

<form class=\"netSiteForm\">
    <div class=\"container\">
        <a href=\"../kariera\" class=\"btn_back\" data-animation=\"slideFromLeft\">
            kariera
        </a>
        <h3 class=\"title\" data-animation=\"fadeIn\">
            Wyślij aplikacje
        </h3>
        
        <form id=\"form\" action=\"\">
            <div class=\"form_row\">
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"name\">Imię i Nazwisko</label>
                    <input id=\"name\" name=\"name\" class=\"formField_el\" type=\"text\" data-required=\"true\">
                </div>
                <div class=\"formField\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"email\">E-mail</label>
                    <input id=\"email\" name=\"email\" class=\"formField_el\" type=\"text\" data-required=\"true\">
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"phoneNumber\">Numer telefonu</label>
                    <input id=\"phoneNumber\" name=\"phoneNumber\" class=\"formField_el\" type=\"text\" data-required=\"true\">
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full\" data-animation=\"fadeIn\">
                    <label class=\"formField_label\" for=\"jobTitle\">Nazwa stanowiska</label>
                    <select id=\"jobTitle\" name=\"jobTitle\" class=\"formField_el\" type=\"text\" data-required=\"true\">
                        <option value=\"none\">Wybierz</option>
                        <option value=\"CNC\">Operator CNC</option>
                    </select>
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_full formField_file\" data-animation=\"fadeIn\">
                    <div class=\"formField_file_row\">
                        <label class=\"formField_label\" for=\"file\">CV</label>
                        <label for=\"file\" class=\"addAttach\">
                            <p>Dodaj załącznik</p>
                            <img src=\"{{ 'career/icon-attachment.svg' | media }}\" alt=\"\">
                        </label>
                        <input id=\"file\" name=\"file\" type=\"file\" class=\"formField_el hiddenInput\" data-required=\"true\">
                    </div>
                    <div class=\"formField_file_row\">
                        <p class=\"fileName\">Jan_Kowalski_CV.pdf</p>
                    </div>
                </div>
            </div>
            <div class=\"form_row\">
                <div class=\"formField formField_agree\">
                    <label class=\"agreeInput\" for=\"agree\" data-animation=\"fadeIn\"></label>
                    <label class=\"formField_label\" for=\"agree\" data-animation=\"fadeIn\">
                        Wyrażam zgodę na wykorzystanie moich danych osobowych przez firmę GranBoard Sp. z o.o.
                        (Administrator danych) w celu udzielenia mi dodatkowych informacji handlowych oraz informacji
                        związanych z zakresem działalności firmy. Podanie danych jest dobrowolne umożliwia uzyskanie w/w
                        informacji. Więcej informacji dotyczących przetwarzania danych znajdą Państwo w naszej polityce
                        prywatności.
                    </label>
                    <input type=\"checkbox\" id=\"agree\" name=\"agree\" hidden data-required=\"true\">
                </div>
            </div>
            <button class=\"submitBTN\" data-animation=\"fadeIn\">Wyślij</button>
        </form>
    </div>
</form>
{% partial 'footer' %}
<script>
    \$('form').on('submit', function () {
        let isok = false;
        const fields = \$('.formField_el[data-required=\"true\"]');
        \$.each(fields, function (index, item) {
            const el = \$(item);
            console.log(el);
        });
        return isok;
    });

    const validation = function (field) {
        const validator = field.attr('data-validator');
        switch (validator) {
            case 'text':
                break;
            case 'email':
                break;
            case 'text':
                break;
        }
    }
</script>
</body>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/career-form.htm", "");
    }
}
