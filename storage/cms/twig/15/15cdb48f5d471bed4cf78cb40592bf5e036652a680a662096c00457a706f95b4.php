<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/layouts/services.htm */
class __TwigTemplate_1fa54f1bb848283c274a53305ddfbeeec438f2f31c6f4049c9575a3e251a9eec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "




























";
        // line 30
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 31
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 32
        echo "
<body class=\"servicesPage\">
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"";
        // line 35
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["hero_img"] ?? null));
        echo "\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"fadeIn\">
                Usługi
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                ";
        // line 44
        echo twig_escape_filter($this->env, ($context["hero_mainText"] ?? null), "html", null, true);
        echo "
            </h1>
        </div>
    </div>
</section>
<section class=\"strokes\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"slideFromLeft\">
            ";
        // line 52
        echo twig_escape_filter($this->env, ($context["strokeText"] ?? null), "html", null, true);
        echo "
        </h3>
    </div>
    <img class=\"shape\" src=\"";
        // line 55
        echo $this->extensions['System\Twig\Extension']->mediaFilter("about_shape.png");
        echo "\" alt=\"\" data-animation=\"slideFromRight\">
</section>
<section id=\"servicesTabs\" class=\"servicesTabs\">
    <div class=\"partImg\" style=\"background: url('";
        // line 58
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["services_bg"] ?? null));
        echo "') center center no-repeat;\">
        ";
        // line 59
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["servicesList"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 60
            echo "        <img class=\"listOnTile_img ";
            if (($context["i"] == 0)) {
                echo "active";
            }
            echo "\" data-service=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 60), "html", null, true);
            echo "\"
             src=\"";
            // line 61
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "img", [], "any", false, false, false, 61));
            echo "\" alt=\"\">
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "        <div class=\"container\">
            <ul class=\"listOnTile\" data-animation=\"fastFadeIn\">
                ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["servicesList"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 66
            echo "
                <li class=\"listOnTile_item ";
            // line 67
            if (($context["i"] == 0)) {
                echo "active";
            }
            echo "\" data-service=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 67), "html", null, true);
            echo "\"
                    data-animation=\"slideFromRight\">
                    ";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 69), "html", null, true);
            echo "
                    <span class=\"circleBtn\"></span>
                </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "            </ul>
            <div class=\"serviceTitle\" data-animation=\"fadeIn\">
                ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["servicesList"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 76
            echo "                <div class=\"serviceTit_item ";
            if (($context["i"] == 0)) {
                echo "active";
            }
            echo "\" data-service=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 76), "html", null, true);
            echo "\">
                    <img class=\"serviceTit_icon\" src=\"";
            // line 77
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "icon", [], "any", false, false, false, 77));
            echo "\" alt=\"\">
                    <h2 class=\"title\">";
            // line 78
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 78), "html", null, true);
            echo "</h2>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "            </div>
        </div>
    </div>
    <div class=\"container\">
        ";
        // line 85
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["servicesList"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 86
            echo "
        <div class=\"serviceContent ";
            // line 87
            if (($context["i"] == 0)) {
                echo "active";
            }
            echo "\" data-service=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 87), "html", null, true);
            echo "\">

            ";
            // line 89
            if ((twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 89) > 0)) {
                // line 90
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 90));
                foreach ($context['_seq'] as $context["_key"] => $context["el"]) {
                    // line 91
                    echo "            <h4 data-animation=\"fadeIn\">
                ";
                    // line 92
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["el"], "title", [], "any", false, false, false, 92), "html", null, true);
                    echo "
            </h4>
            <p data-animation=\"fadeIn\">
                ";
                    // line 95
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["el"], "text", [], "any", false, false, false, 95), "html", null, true);
                    echo "
            </p>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['el'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 98
                echo "            ";
            }
            // line 99
            echo "
            ";
            // line 100
            if ((twig_get_attribute($this->env, $this->source, $context["item"], "scope_of_service", [], "any", false, false, false, 100) > 0)) {
                // line 101
                echo "            <h4 data-animation=\"fadeIn\">
                Zakres usługi
            </h4>
            <ul data-animation=\"fadeIn\">
                ";
                // line 105
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "scope_of_service", [], "any", false, false, false, 105));
                foreach ($context['_seq'] as $context["_key"] => $context["el"]) {
                    // line 106
                    echo "                <li>
                    <span class=\"check_icon\"></span>
                    ";
                    // line 108
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["el"], "text", [], "any", false, false, false, 108), "html", null, true);
                    echo "
                </li>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['el'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 111
                echo "            </ul>
            ";
            }
            // line 113
            echo "        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"";
        // line 121
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["imgWithText_1_img"] ?? null));
        echo "\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        ";
        // line 126
        echo twig_escape_filter($this->env, ($context["imgWithText_1_title"] ?? null), "html", null, true);
        echo "
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        ";
        // line 129
        echo twig_escape_filter($this->env, ($context["imgWithText_1_text"] ?? null), "html", null, true);
        echo "
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"";
        // line 140
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["imgWithText_2_img"] ?? null));
        echo "\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        ";
        // line 145
        echo twig_escape_filter($this->env, ($context["imgWithText_2_title"] ?? null), "html", null, true);
        echo "
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        ";
        // line 148
        echo twig_escape_filter($this->env, ($context["imgWithText_2_text"] ?? null), "html", null, true);
        echo "
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText no-text-padding\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"";
        // line 159
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["imgWithText_3_img"] ?? null));
        echo "\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        ";
        // line 164
        echo twig_escape_filter($this->env, ($context["imgWithText_3_title"] ?? null), "html", null, true);
        echo "
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        ";
        // line 167
        echo twig_escape_filter($this->env, ($context["imgWithText_3_text"] ?? null), "html", null, true);
        echo "
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"";
        // line 178
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["imgWithText_5_img"] ?? null));
        echo "\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        ";
        // line 183
        echo twig_escape_filter($this->env, ($context["imgWithText_5_title"] ?? null), "html", null, true);
        echo "
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        ";
        // line 186
        echo twig_escape_filter($this->env, ($context["imgWithText_5_text"] ?? null), "html", null, true);
        echo "
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"fadeIn\">
            ";
        // line 196
        echo twig_escape_filter($this->env, ($context["btnWithText_title"] ?? null), "html", null, true);
        echo "
        </h3>
        <p class=\"text\" data-animation=\"fadeIn\">
            ";
        // line 199
        echo twig_escape_filter($this->env, ($context["btnWithText_text"] ?? null), "html", null, true);
        echo "
        </p>
        <a class=\"arrow_btn_bg\" href=\"\" data-animation=\"fadeIn\">
            PRZEJDŹ DO KONTAKTU
        </a>
    </div>
</section>

<script>

    const href = window.location.href.split('#')[1];

    if (href != undefined) {
        set_activeService(href);
        scroll_activeService();
    }
    \$(document).ready(function () {
        \$('.listOnTile_item').on('click', function () {
            const service = \$(this).attr('data-service');
            set_activeService(service);
        });
    });


    function scroll_set_activeService(name) {
        scroll_activeService();
        set_activeService(name);
    }

    function scroll_activeService() {
        const scrollTo = document.getElementById('servicesTabs');
        console.log(scrollTo.offsetTop);
        window.scroll({
            behavior: 'smooth',
            left: 0,
            top: scrollTo.offsetTop
        });
    }

    function set_activeService(name) {
        console.log(\$('#servicesTabs *[data-service=\"' + name + '\"]'));
        \$('#servicesTabs [data-service]').removeClass('active');
        \$('#servicesTabs *[data-service=\"' + name + '\"]').addClass('active');
    }
</script>
";
        // line 244
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 245
        echo "</body>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/services.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  467 => 245,  463 => 244,  415 => 199,  409 => 196,  396 => 186,  390 => 183,  382 => 178,  368 => 167,  362 => 164,  354 => 159,  340 => 148,  334 => 145,  326 => 140,  312 => 129,  306 => 126,  298 => 121,  290 => 115,  283 => 113,  279 => 111,  270 => 108,  266 => 106,  262 => 105,  256 => 101,  254 => 100,  251 => 99,  248 => 98,  239 => 95,  233 => 92,  230 => 91,  225 => 90,  223 => 89,  214 => 87,  211 => 86,  207 => 85,  201 => 81,  192 => 78,  188 => 77,  179 => 76,  175 => 75,  171 => 73,  161 => 69,  152 => 67,  149 => 66,  145 => 65,  141 => 63,  133 => 61,  124 => 60,  120 => 59,  116 => 58,  110 => 55,  104 => 52,  93 => 44,  81 => 35,  76 => 32,  72 => 31,  68 => 30,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("




























{% partial 'header' %}
{% partial 'navigation' %}

<body class=\"servicesPage\">
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"{{ hero_img | media }}\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"fadeIn\">
                Usługi
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                {{hero_mainText}}
            </h1>
        </div>
    </div>
</section>
<section class=\"strokes\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"slideFromLeft\">
            {{strokeText}}
        </h3>
    </div>
    <img class=\"shape\" src=\"{{ 'about_shape.png' | media }}\" alt=\"\" data-animation=\"slideFromRight\">
</section>
<section id=\"servicesTabs\" class=\"servicesTabs\">
    <div class=\"partImg\" style=\"background: url('{{ services_bg | media }}') center center no-repeat;\">
        {% for i,item in servicesList %}
        <img class=\"listOnTile_img {% if(i==0)%}active{% endif %}\" data-service=\"{{item.slug}}\"
             src=\"{{item.img | media }}\" alt=\"\">
        {% endfor %}
        <div class=\"container\">
            <ul class=\"listOnTile\" data-animation=\"fastFadeIn\">
                {% for i,item in servicesList %}

                <li class=\"listOnTile_item {% if(i==0)%}active{% endif %}\" data-service=\"{{item.slug}}\"
                    data-animation=\"slideFromRight\">
                    {{item.name}}
                    <span class=\"circleBtn\"></span>
                </li>
                {% endfor %}
            </ul>
            <div class=\"serviceTitle\" data-animation=\"fadeIn\">
                {% for i,item in servicesList %}
                <div class=\"serviceTit_item {% if(i==0)%}active{% endif %}\" data-service=\"{{item.slug}}\">
                    <img class=\"serviceTit_icon\" src=\"{{ item.icon | media }}\" alt=\"\">
                    <h2 class=\"title\">{{ item.name }}</h2>
                </div>
                {% endfor %}
            </div>
        </div>
    </div>
    <div class=\"container\">
        {% for i,item in servicesList %}

        <div class=\"serviceContent {% if(i==0)%}active{% endif %}\" data-service=\"{{item.slug}}\">

            {% if(item.description > 0) %}
            {% for el in item.description %}
            <h4 data-animation=\"fadeIn\">
                {{el.title}}
            </h4>
            <p data-animation=\"fadeIn\">
                {{el.text}}
            </p>
            {% endfor %}
            {% endif %}

            {% if(item.scope_of_service > 0) %}
            <h4 data-animation=\"fadeIn\">
                Zakres usługi
            </h4>
            <ul data-animation=\"fadeIn\">
                {% for el in item.scope_of_service %}
                <li>
                    <span class=\"check_icon\"></span>
                    {{el.text}}
                </li>
                {% endfor %}
            </ul>
            {% endif %}
        </div>
        {% endfor %}
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"{{ imgWithText_1_img | media }}\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        {{imgWithText_1_title}}
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        {{imgWithText_1_text}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"{{ imgWithText_2_img | media }}\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        {{ imgWithText_2_title }}
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        {{ imgWithText_2_text }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText no-text-padding\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"{{ imgWithText_3_img | media }}\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        {{ imgWithText_3_title }}
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        {{ imgWithText_3_text }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"{{ imgWithText_5_img | media }}\" alt=\"\" data-animation=\"slideFromLeft\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        {{ imgWithText_5_title }}
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        {{ imgWithText_5_text }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"fadeIn\">
            {{btnWithText_title}}
        </h3>
        <p class=\"text\" data-animation=\"fadeIn\">
            {{btnWithText_text}}
        </p>
        <a class=\"arrow_btn_bg\" href=\"\" data-animation=\"fadeIn\">
            PRZEJDŹ DO KONTAKTU
        </a>
    </div>
</section>

<script>

    const href = window.location.href.split('#')[1];

    if (href != undefined) {
        set_activeService(href);
        scroll_activeService();
    }
    \$(document).ready(function () {
        \$('.listOnTile_item').on('click', function () {
            const service = \$(this).attr('data-service');
            set_activeService(service);
        });
    });


    function scroll_set_activeService(name) {
        scroll_activeService();
        set_activeService(name);
    }

    function scroll_activeService() {
        const scrollTo = document.getElementById('servicesTabs');
        console.log(scrollTo.offsetTop);
        window.scroll({
            behavior: 'smooth',
            left: 0,
            top: scrollTo.offsetTop
        });
    }

    function set_activeService(name) {
        console.log(\$('#servicesTabs *[data-service=\"' + name + '\"]'));
        \$('#servicesTabs [data-service]').removeClass('active');
        \$('#servicesTabs *[data-service=\"' + name + '\"]').addClass('active');
    }
</script>
{% partial 'footer' %}
</body>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/services.htm", "");
    }
}
