<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/layouts/mainPage.htm */
class __TwigTemplate_883efe615d56bbad6f9829ca0d4e1119c06a8a8c1474f9b41e5da4d261e597d9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "



";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["extraData"] ?? null), "technologies", [], "any", false, false, false, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["fields"]) {
            // line 6
            echo "


";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "














";
        // line 25
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 25);
        // line 26
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 26);
        // line 27
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 27);
        // line 28
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 28);
        // line 29
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 29);
        // line 30
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 30);
        // line 31
        echo "
";
        // line 32
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 32);
        // line 33
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 33);
        // line 34
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 34);
        // line 35
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 35);
        // line 36
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 36);
        // line 37
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 37);
        // line 38
        echo "

";
        // line 40
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 41
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 42
        echo "<body class=\"mainPage\">
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"";
        // line 44
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["hero_img"] ?? null));
        echo "\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <h1 class=\"title\" data-animation=\"fadeIn\">
                ";
        // line 50
        echo ($context["hero_mainText"] ?? null);
        echo "
            </h1>

        </div>
        <div class=\"row\">
            <a href=\"\" class=\"arrow_btn\" data-animation=\"fadeIn\">zobacz naszą ofertę</a>
        </div>
    </div>
</section>
<section class=\"offer\">
    <div class=\"container\">
        <div class=\"row textCenter\">
            <h2 class=\"title\" data-animation=\"fadeIn\">
                ";
        // line 63
        echo twig_escape_filter($this->env, ($context["title_technology"] ?? null), "html", null, true);
        echo "
            </h2>
        </div>
        <div class=\"row offer-row\">

            ";
        // line 68
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["servicesList"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 69
            echo "            <a href=\"uslugi#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 69), "html", null, true);
            echo "\" class=\"tile\" data-animation=\"fadeIn\">
                <img src=\"";
            // line 70
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "mainpage_img", [], "any", false, false, false, 70));
            echo "\" alt=\"\" class=\"tile-img\">
                <p class=\"tile-title\">";
            // line 71
            echo twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 71);
            echo "</p>
            </a>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "
        </div>
    </div>
</section>
<section class=\"qualityStandard\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-content\">
                <img data-animation=\"slideFromLeft\" src=\"";
        // line 82
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["section_quality_img"] ?? null));
        echo "\" alt=\"\">
            </div>
            <div class=\"col-content\">
                <h2 class=\"title\" data-animation=\"fadeIn\">
                    ";
        // line 86
        echo twig_escape_filter($this->env, ($context["section_quality_mainText"] ?? null), "html", null, true);
        echo "
                </h2>
                <p class=\"content\" data-animation=\"fadeIn\">
                    ";
        // line 89
        echo twig_escape_filter($this->env, ($context["section_quality_description"] ?? null), "html", null, true);
        echo "
                </p>
                <a class=\"arrow_btn_bg\" data-animation=\"fadeIn\" href=\"\">zobacz nasz certyfikaty</a>
            </div>
        </div>
    </div>
</section>
<section class=\"meetUs\">
    <img class=\"shiftedTile_bg\" src=\"";
        // line 97
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["section_meetUS_img"] ?? null));
        echo "\" alt=\"\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"img_col\" data-animation=\"slideFromLeft\">
                <div class=\"box\">
                    <img src=\"";
        // line 102
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["section_meetUS_icon"] ?? null));
        echo "\" alt=\"\">
                </div>
            </div>
            <div class=\"tile_col\" data-animation=\"slideFromRight\">

                <p class=\"text\">
                    ";
        // line 108
        echo twig_escape_filter($this->env, ($context["section_meetUS_mainText"] ?? null), "html", null, true);
        echo "
                </p>
                <a href=\"about\" class=\"arrow_btn\">poznaj nas</a>

            </div>
        </div>
    </div>
</section>
<section class=\"ourProducts\" data-animation=\"fadeIn\">
    <div class=\"container\">
        <div class=\"row textCenter\">
            <h2 class=\"title\">
                Nasze produkty
            </h2>
        </div>
        <div class=\"row\">
            <div class=\"col\">
                <img class=\"placeholder\" src=\"";
        // line 125
        echo $this->extensions['System\Twig\Extension']->mediaFilter("productPlaveholder.png");
        echo "\" alt=\"\">
                ";
        // line 126
        $context["counter"] = 0;
        // line 127
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["index"] => $context["item"]) {
            // line 128
            echo "                ";
            if (twig_get_attribute($this->env, $this->source, $context["item"], "mainpage_show", [], "any", false, false, false, 128)) {
                // line 129
                echo "                <img class=\"productImg ";
                if (($context["index"] == 0)) {
                    echo "active";
                }
                echo "\" imgId=\"";
                echo twig_escape_filter($this->env, ($context["counter"] ?? null), "html", null, true);
                echo "\"
                     src=\"";
                // line 130
                echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "main_img", [], "any", false, false, false, 130));
                echo "\" alt=\"\">
                ";
                // line 131
                $context["counter"] = (($context["counter"] ?? null) + 1);
                // line 132
                echo "                ";
            }
            // line 133
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 134
        echo "            </div>
            <div class=\"col\">
                <ul class=\"productList\">
                    ";
        // line 137
        $context["counter"] = 0;
        // line 138
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["index"] => $context["item"]) {
            // line 139
            echo "                    ";
            if (twig_get_attribute($this->env, $this->source, $context["item"], "mainpage_show", [], "any", false, false, false, 139)) {
                // line 140
                echo "                    <li class=\"productList_el ";
                if (($context["index"] == 0)) {
                    echo "active";
                }
                echo "\" productId=\"";
                echo twig_escape_filter($this->env, ($context["counter"] ?? null), "html", null, true);
                echo "\">
                        <p class=\"product_name\">";
                // line 141
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 141), "html", null, true);
                echo "</p>
                        <p class=\"product_description\">";
                // line 142
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "mainpage_expand_shortcut", [], "any", false, false, false, 142), "html", null, true);
                echo "</p>
                        <div class=\"line-row\">
                            <a href=\"\" class=\"circle_btn\"></a>
                            <div class=\"line\"></div>
                        </div>
                    </li>
                    ";
                // line 148
                $context["counter"] = (($context["counter"] ?? null) + 1);
                // line 149
                echo "                    ";
            }
            // line 150
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 151
        echo "                </ul>
            </div>
        </div>
        <div class=\"row textCenter\">
            <a href=\"produkty\" class=\"arrow_btn\">zobacz pełną ofertę produktów</a>
        </div>
    </div>
</section>
<section class=\"machinePark\">
    <img class=\"shiftedTile_bg\" src=\"";
        // line 160
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["section_machinePark_img"] ?? null));
        echo "\" alt=\"\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"img_col\">
                <div class=\"box\">

                </div>
            </div>
            <div class=\"tile_col\" data-animation=\"slideFromRight\">

                <p class=\"text\">
                    ";
        // line 171
        echo twig_escape_filter($this->env, ($context["section_machinePark_mainText"] ?? null), "html", null, true);
        echo "
                </p>
                <a href=\"\" class=\"arrow_btn\">zobacz nasz park maszynowy</a>

            </div>
        </div>
    </div>
</section>
<section class=\"seeMore\">
    <div class=\"container\">
        <div class=\"row textCenter\">
            <h2 class=\"title\" data-animation=\"slideFromLeft\">
                ";
        // line 183
        echo twig_escape_filter($this->env, ($context["section_moreProduct_mainText"] ?? null), "html", null, true);
        echo "
            </h2>
        </div>
        <div class=\"row textCenter\" data-animation=\"slideFromRight\">
            <p class=\"text\">
                ";
        // line 188
        echo twig_escape_filter($this->env, ($context["section_moreProduct_description"] ?? null), "html", null, true);
        echo "
            </p>
        </div>
        <div class=\"row textCenter\">
            <a href=\"onas\" class=\"arrow_btn_bg\" data-animation=\"fadeIn\">poznaj nas</a>
        </div>
    </div>
</section>

<script>
    const ourProducts = Array.from(document.querySelectorAll('.productList_el'));
    const ourProductsImg = Array.from(document.querySelectorAll('.productImg'));

    ourProducts.forEach(function (el) {
        el.addEventListener('click', function (e) {
            const target = e.target.closest('.productList_el').getAttribute('productId');
            console.log(target);
            ourProductsImg.forEach(function (el) {
                el.classList.remove('active');
            });
            ourProducts.forEach(function (el) {
                el.classList.remove('active');
            });
            document.querySelector('.productImg[imgId=\"' + target + '\"]').classList.add('active')
            e.target.closest('.productList_el').classList.add('active');
        });
    });
</script>
";
        // line 216
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 217
        echo "</body>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/mainPage.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  411 => 217,  407 => 216,  376 => 188,  368 => 183,  353 => 171,  339 => 160,  328 => 151,  322 => 150,  319 => 149,  317 => 148,  308 => 142,  304 => 141,  295 => 140,  292 => 139,  287 => 138,  285 => 137,  280 => 134,  274 => 133,  271 => 132,  269 => 131,  265 => 130,  256 => 129,  253 => 128,  248 => 127,  246 => 126,  242 => 125,  222 => 108,  213 => 102,  205 => 97,  194 => 89,  188 => 86,  181 => 82,  171 => 74,  162 => 71,  158 => 70,  153 => 69,  149 => 68,  141 => 63,  125 => 50,  116 => 44,  112 => 42,  108 => 41,  104 => 40,  100 => 38,  98 => 37,  96 => 36,  94 => 35,  92 => 34,  90 => 33,  88 => 32,  85 => 31,  83 => 30,  81 => 29,  79 => 28,  77 => 27,  75 => 26,  73 => 25,  56 => 10,  47 => 6,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("



{% for fields in extraData.technologies %}



{% endfor %}















{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}


{% partial 'header' %}
{% partial 'navigation' %}
<body class=\"mainPage\">
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"{{ hero_img | media }}\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <h1 class=\"title\" data-animation=\"fadeIn\">
                {{ hero_mainText | raw }}
            </h1>

        </div>
        <div class=\"row\">
            <a href=\"\" class=\"arrow_btn\" data-animation=\"fadeIn\">zobacz naszą ofertę</a>
        </div>
    </div>
</section>
<section class=\"offer\">
    <div class=\"container\">
        <div class=\"row textCenter\">
            <h2 class=\"title\" data-animation=\"fadeIn\">
                {{ title_technology }}
            </h2>
        </div>
        <div class=\"row offer-row\">

            {% for i,item in servicesList %}
            <a href=\"uslugi#{{item.slug}}\" class=\"tile\" data-animation=\"fadeIn\">
                <img src=\"{{ item.mainpage_img | media }}\" alt=\"\" class=\"tile-img\">
                <p class=\"tile-title\">{{ item.name | raw }}</p>
            </a>
            {% endfor %}

        </div>
    </div>
</section>
<section class=\"qualityStandard\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-content\">
                <img data-animation=\"slideFromLeft\" src=\"{{ section_quality_img | media }}\" alt=\"\">
            </div>
            <div class=\"col-content\">
                <h2 class=\"title\" data-animation=\"fadeIn\">
                    {{section_quality_mainText}}
                </h2>
                <p class=\"content\" data-animation=\"fadeIn\">
                    {{section_quality_description}}
                </p>
                <a class=\"arrow_btn_bg\" data-animation=\"fadeIn\" href=\"\">zobacz nasz certyfikaty</a>
            </div>
        </div>
    </div>
</section>
<section class=\"meetUs\">
    <img class=\"shiftedTile_bg\" src=\"{{ section_meetUS_img | media }}\" alt=\"\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"img_col\" data-animation=\"slideFromLeft\">
                <div class=\"box\">
                    <img src=\"{{ section_meetUS_icon | media }}\" alt=\"\">
                </div>
            </div>
            <div class=\"tile_col\" data-animation=\"slideFromRight\">

                <p class=\"text\">
                    {{ section_meetUS_mainText }}
                </p>
                <a href=\"about\" class=\"arrow_btn\">poznaj nas</a>

            </div>
        </div>
    </div>
</section>
<section class=\"ourProducts\" data-animation=\"fadeIn\">
    <div class=\"container\">
        <div class=\"row textCenter\">
            <h2 class=\"title\">
                Nasze produkty
            </h2>
        </div>
        <div class=\"row\">
            <div class=\"col\">
                <img class=\"placeholder\" src=\"{{ 'productPlaveholder.png' | media}}\" alt=\"\">
                {% set counter = 0 %}
                {% for index,item in records %}
                {% if(item.mainpage_show)%}
                <img class=\"productImg {% if(index == 0)%}active{% endif %}\" imgId=\"{{counter}}\"
                     src=\"{{ item.main_img | media }}\" alt=\"\">
                {% set counter = counter + 1 %}
                {% endif %}
                {% endfor %}
            </div>
            <div class=\"col\">
                <ul class=\"productList\">
                    {% set counter = 0 %}
                    {% for index,item in records %}
                    {% if(item.mainpage_show)%}
                    <li class=\"productList_el {% if(index == 0)%}active{% endif %}\" productId=\"{{counter}}\">
                        <p class=\"product_name\">{{item.name}}</p>
                        <p class=\"product_description\">{{item.mainpage_expand_shortcut}}</p>
                        <div class=\"line-row\">
                            <a href=\"\" class=\"circle_btn\"></a>
                            <div class=\"line\"></div>
                        </div>
                    </li>
                    {% set counter = counter + 1 %}
                    {% endif %}
                    {% endfor %}
                </ul>
            </div>
        </div>
        <div class=\"row textCenter\">
            <a href=\"produkty\" class=\"arrow_btn\">zobacz pełną ofertę produktów</a>
        </div>
    </div>
</section>
<section class=\"machinePark\">
    <img class=\"shiftedTile_bg\" src=\"{{ section_machinePark_img | media }}\" alt=\"\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"img_col\">
                <div class=\"box\">

                </div>
            </div>
            <div class=\"tile_col\" data-animation=\"slideFromRight\">

                <p class=\"text\">
                    {{ section_machinePark_mainText }}
                </p>
                <a href=\"\" class=\"arrow_btn\">zobacz nasz park maszynowy</a>

            </div>
        </div>
    </div>
</section>
<section class=\"seeMore\">
    <div class=\"container\">
        <div class=\"row textCenter\">
            <h2 class=\"title\" data-animation=\"slideFromLeft\">
                {{ section_moreProduct_mainText }}
            </h2>
        </div>
        <div class=\"row textCenter\" data-animation=\"slideFromRight\">
            <p class=\"text\">
                {{ section_moreProduct_description }}
            </p>
        </div>
        <div class=\"row textCenter\">
            <a href=\"onas\" class=\"arrow_btn_bg\" data-animation=\"fadeIn\">poznaj nas</a>
        </div>
    </div>
</section>

<script>
    const ourProducts = Array.from(document.querySelectorAll('.productList_el'));
    const ourProductsImg = Array.from(document.querySelectorAll('.productImg'));

    ourProducts.forEach(function (el) {
        el.addEventListener('click', function (e) {
            const target = e.target.closest('.productList_el').getAttribute('productId');
            console.log(target);
            ourProductsImg.forEach(function (el) {
                el.classList.remove('active');
            });
            ourProducts.forEach(function (el) {
                el.classList.remove('active');
            });
            document.querySelector('.productImg[imgId=\"' + target + '\"]').classList.add('active')
            e.target.closest('.productList_el').classList.add('active');
        });
    });
</script>
{% partial 'footer' %}
</body>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/mainPage.htm", "");
    }
}
