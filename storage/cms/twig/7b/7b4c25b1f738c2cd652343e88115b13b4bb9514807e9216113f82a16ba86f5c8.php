<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* D:\xampp\htdocs\granboard_cms_app/themes/grandboard/layouts/about.htm */
class __TwigTemplate_0c7a34feaf5b9de5e2655664c0c9663439c73bb4e795be4709caba434a1486c6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "






















";
        // line 24
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 25
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 26
        echo "<body class=\"aboutPage\">
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"";
        // line 28
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["hero_img"] ?? null));
        echo "\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"fadeIn\">
                Granboard
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                ";
        // line 37
        echo twig_escape_filter($this->env, ($context["hero_mainText"] ?? null), "html", null, true);
        echo "
            </h1>
        </div>
    </div>
</section>
<section class=\"strokes\">

    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"slideFromLeft\">
            ";
        // line 46
        echo twig_escape_filter($this->env, ($context["strokeText"] ?? null), "html", null, true);
        echo "
        </h3>
    </div>
    <img class=\"shape\" src=\"";
        // line 49
        echo $this->extensions['System\Twig\Extension']->mediaFilter("about_shape.png");
        echo "\" alt=\"\" data-animation=\"slideFromRight\">
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"";
        // line 55
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["imgText_1_img"] ?? null));
        echo "\" alt=\"\" data-animation=\"fadeIn\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        ";
        // line 60
        echo twig_escape_filter($this->env, ($context["imgText_1_title"] ?? null), "html", null, true);
        echo "
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        ";
        // line 63
        echo twig_escape_filter($this->env, ($context["imgText_1_text"] ?? null), "html", null, true);
        echo "
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"slideFromLeft\">
            ";
        // line 73
        echo twig_escape_filter($this->env, ($context["btnText_1_title"] ?? null), "html", null, true);
        echo "
        </h3>
        <p class=\"text\" data-animation=\"slideFromRight\">
            ";
        // line 76
        echo twig_escape_filter($this->env, ($context["btnText_1_text"] ?? null), "html", null, true);
        echo "
        </p>
        <a class=\"arrow_btn_bg\" href=\"produkty\" data-animation=\"fadeIn\">
            Sprawdź naszą ofertę
        </a>
    </div>
</section>
<div class=\"servicesSlider\">
    <div class=\"container\" data-animation=\"fadeIn\">
        <h2 class=\"sectionTitle\">";
        // line 85
        echo twig_escape_filter($this->env, ($context["services_title"] ?? null), "html", null, true);
        echo "</h2>
        <div class=\"serviceList\">
            ";
        // line 87
        $context["counter"] = 0;
        // line 88
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["servicesList"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 89
            echo "            ";
            if (twig_get_attribute($this->env, $this->source, $context["item"], "about_show", [], "any", false, false, false, 89)) {
                // line 90
                echo "            <div class=\"serviceList_item ";
                if (($context["i"] == 0)) {
                    echo "active";
                }
                echo "\" data-slide=\"";
                echo twig_escape_filter($this->env, ($context["counter"] ?? null), "html", null, true);
                echo "\">
                <p class=\"service_name\">";
                // line 91
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 91), "html", null, true);
                echo "</p>
                <p class=\"service_description\">
                    ";
                // line 93
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "about_subtitle", [], "any", false, false, false, 93), "html", null, true);
                echo "
                </p>
            </div>
            ";
                // line 96
                $context["counter"] = (($context["counter"] ?? null) + 1);
                // line 97
                echo "            ";
            }
            // line 98
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "        </div>
    </div>
    <div class=\"servicesSlider_slides\">

        <div class=\"contentSide\" data-animation=\"slideFromLeft\">
            <div class=\"servicesSlider_nav\">
                <div class=\"navLeft\"></div>
                <div class=\"navRight\"></div>
            </div>
            ";
        // line 108
        $context["counter"] = 0;
        // line 109
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["servicesList"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 110
            echo "            ";
            if (twig_get_attribute($this->env, $this->source, $context["item"], "about_show", [], "any", false, false, false, 110)) {
                // line 111
                echo "            <div class=\"contentSide_slide ";
                if (($context["i"] == 0)) {
                    echo "active";
                }
                echo "\" data-slide=\"";
                echo twig_escape_filter($this->env, ($context["counter"] ?? null), "html", null, true);
                echo "\">
                <h3 class=\"title\">
                    ";
                // line 113
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 113), "html", null, true);
                echo "
                </h3>
                <p class=\"text\">
                    ";
                // line 116
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "about_description", [], "any", false, false, false, 116), "html", null, true);
                echo "
                </p>
                <a href=\"services#";
                // line 118
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "slug", [], "any", false, false, false, 118), "html", null, true);
                echo "\" class=\"arrow_btn\">ZOBACZ ZAKRES OFERTY</a>
            </div>
            ";
                // line 120
                $context["counter"] = (($context["counter"] ?? null) + 1);
                // line 121
                echo "            ";
            }
            // line 122
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "        </div>
        <div class=\"imgSide\" data-animation=\"slideFromRight\">
            ";
        // line 125
        $context["counter"] = 0;
        // line 126
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["servicesList"] ?? null));
        foreach ($context['_seq'] as $context["i"] => $context["item"]) {
            // line 127
            echo "            ";
            if (twig_get_attribute($this->env, $this->source, $context["item"], "about_show", [], "any", false, false, false, 127)) {
                // line 128
                echo "            <img class=\"imgSide_item ";
                if (($context["i"] == 0)) {
                    echo "active";
                }
                echo "\" data-slide=\"";
                echo twig_escape_filter($this->env, ($context["counter"] ?? null), "html", null, true);
                echo "\"
                 src=\"";
                // line 129
                echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["item"], "about_img", [], "any", false, false, false, 129));
                echo "\"
                 alt=\"\">
            ";
                // line 131
                $context["counter"] = (($context["counter"] ?? null) + 1);
                // line 132
                echo "            ";
            }
            // line 133
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 134
        echo "        </div>
    </div>
</div>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\" data-animation=\"slideFromLeft\">
                <img src=\"";
        // line 141
        echo $this->extensions['System\Twig\Extension']->mediaFilter(($context["imgText_2_img"] ?? null));
        echo "\" alt=\"\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        ";
        // line 146
        echo twig_escape_filter($this->env, ($context["imgText_2_title"] ?? null), "html", null, true);
        echo "
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        ";
        // line 149
        echo twig_escape_filter($this->env, ($context["imgText_2_text"] ?? null), "html", null, true);
        echo "
                    </p>
                    <a class=\"arrow_btn_bg\" href=\"kariera\" data-animation=\"slideFromRight\">Zobacz OFERTY PRACY</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"fadeIn\">
            ";
        // line 160
        echo twig_escape_filter($this->env, ($context["btnText_2_title"] ?? null), "html", null, true);
        echo "
        </h3>
        <p class=\"text\" data-animation=\"fadeIn\">
            ";
        // line 163
        echo twig_escape_filter($this->env, ($context["btnText_2_text"] ?? null), "html", null, true);
        echo "
        </p>
        <a class=\"arrow_btn_bg\" href=\"kariera\" data-animation=\"fadeIn\">
            KARIERA W GRANBOARD
        </a>
    </div>
</section>
<script>
    const serviceList = Array.from(document.querySelectorAll('.serviceList_item'));
    const navLeft = document.querySelector('.navLeft');
    const navRight = document.querySelector('.navRight');

    serviceList.forEach(function (item) {
        item.addEventListener('click', function (e) {
            const activeIndex = e.target.closest('.serviceList_item').getAttribute('data-slide');
            set_activeSlide(activeIndex);
        });
    });
    navLeft.addEventListener('click', function () {
        let activeIndex = get_activeIndex();
        activeIndex = activeIndex - 1 < 0 ? serviceList.length - 1 : activeIndex - 1;
        set_activeSlide(activeIndex);
    });
    navRight.addEventListener('click', function () {
        let activeIndex = get_activeIndex();
        activeIndex = activeIndex + 1 > serviceList.length - 1 ? 0 : activeIndex + 1;
        set_activeSlide(activeIndex);
    });

    function get_activeIndex() {
        const slider = get_slideData();

        let activeIndex = -1;
        slider.forEach(function (el, index) {
            if (el.active)
                activeIndex = index;
        });
        return activeIndex;

    }

    function get_slideData() {
        const slide_list = Array.from(document.querySelectorAll('.serviceList_item'));
        const slide_content = Array.from(document.querySelectorAll('.contentSide_slide'));
        const slide_img = Array.from(document.querySelectorAll('.imgSide_item'));
        let data = [];
        slide_list.forEach(function (el, index) {

            data.push({
                slide_item: el,
                slide_content: slide_content[index],
                slide_img: slide_img[index],
                active: el.classList.contains('active')
            })
        });
        return data;
    }

    function set_activeSlide(activeIndex) {
        let slider = get_slideData();
        activeIndex = parseInt(activeIndex);
        if (!slider[activeIndex].active) {
            slider.forEach(function (el, index) {
                const slide_item = el.slide_item;
                const slide_content = el.slide_content;
                const slide_img = el.slide_img;
                if (index === activeIndex) {
                    slide_item.classList.add('active');
                    slide_content.classList.add('active');
                    slide_img.classList.add('active');
                    slide_img.active = true;
                } else {
                    slide_item.classList.remove('active');
                    slide_content.classList.remove('active');
                    slide_img.classList.remove('active');
                    slide_img.active = false;
                }
            });
        }
    }

</script>
";
        // line 245
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 246
        echo "</body>";
    }

    public function getTemplateName()
    {
        return "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/about.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  433 => 246,  429 => 245,  344 => 163,  338 => 160,  324 => 149,  318 => 146,  310 => 141,  301 => 134,  295 => 133,  292 => 132,  290 => 131,  285 => 129,  276 => 128,  273 => 127,  268 => 126,  266 => 125,  262 => 123,  256 => 122,  253 => 121,  251 => 120,  246 => 118,  241 => 116,  235 => 113,  225 => 111,  222 => 110,  217 => 109,  215 => 108,  204 => 99,  198 => 98,  195 => 97,  193 => 96,  187 => 93,  182 => 91,  173 => 90,  170 => 89,  165 => 88,  163 => 87,  158 => 85,  146 => 76,  140 => 73,  127 => 63,  121 => 60,  113 => 55,  104 => 49,  98 => 46,  86 => 37,  74 => 28,  70 => 26,  66 => 25,  62 => 24,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("






















{% partial 'header' %}
{% partial 'navigation' %}
<body class=\"aboutPage\">
<section class=\"hero\" data-animation=\"fastFadeIn\">
    <img class=\"mainImg mainImg_desktop\" src=\"{{ hero_img | media}}\" alt=\"\">
    <div class=\"mainImg-dark\"></div>
    <div class=\"leftBar\" data-animation=\"slideFromLeft\"></div>
    <div class=\"container\">
        <div class=\"row\">
            <p class=\"subTitle\" data-animation=\"fadeIn\">
                Granboard
            </p>
            <h1 class=\"title\" data-animation=\"fadeIn\">
                {{hero_mainText}}
            </h1>
        </div>
    </div>
</section>
<section class=\"strokes\">

    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"slideFromLeft\">
            {{strokeText}}
        </h3>
    </div>
    <img class=\"shape\" src=\"{{ 'about_shape.png' | media }}\" alt=\"\" data-animation=\"slideFromRight\">
</section>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <img src=\"{{ imgText_1_img | media }}\" alt=\"\" data-animation=\"fadeIn\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        {{imgText_1_title}}
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        {{imgText_1_text}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"slideFromLeft\">
            {{btnText_1_title}}
        </h3>
        <p class=\"text\" data-animation=\"slideFromRight\">
            {{btnText_1_text}}
        </p>
        <a class=\"arrow_btn_bg\" href=\"produkty\" data-animation=\"fadeIn\">
            Sprawdź naszą ofertę
        </a>
    </div>
</section>
<div class=\"servicesSlider\">
    <div class=\"container\" data-animation=\"fadeIn\">
        <h2 class=\"sectionTitle\">{{services_title}}</h2>
        <div class=\"serviceList\">
            {% set counter = 0 %}
            {% for i,item in servicesList %}
            {% if(item.about_show) %}
            <div class=\"serviceList_item {% if(i == 0)%}active{% endif %}\" data-slide=\"{{counter}}\">
                <p class=\"service_name\">{{item.name}}</p>
                <p class=\"service_description\">
                    {{ item.about_subtitle }}
                </p>
            </div>
            {% set counter = counter+1 %}
            {% endif %}
            {% endfor %}
        </div>
    </div>
    <div class=\"servicesSlider_slides\">

        <div class=\"contentSide\" data-animation=\"slideFromLeft\">
            <div class=\"servicesSlider_nav\">
                <div class=\"navLeft\"></div>
                <div class=\"navRight\"></div>
            </div>
            {% set counter = 0 %}
            {% for i,item in servicesList %}
            {% if(item.about_show) %}
            <div class=\"contentSide_slide {% if(i == 0)%}active{% endif %}\" data-slide=\"{{counter}}\">
                <h3 class=\"title\">
                    {{ item.name }}
                </h3>
                <p class=\"text\">
                    {{ item.about_description }}
                </p>
                <a href=\"services#{{item.slug}}\" class=\"arrow_btn\">ZOBACZ ZAKRES OFERTY</a>
            </div>
            {% set counter = counter+1 %}
            {% endif %}
            {% endfor %}
        </div>
        <div class=\"imgSide\" data-animation=\"slideFromRight\">
            {% set counter = 0 %}
            {% for i,item in servicesList %}
            {% if(item.about_show) %}
            <img class=\"imgSide_item {% if(i == 0)%}active{% endif %}\" data-slide=\"{{counter}}\"
                 src=\"{{ item.about_img | media}}\"
                 alt=\"\">
            {% set counter = counter+1 %}
            {% endif %}
            {% endfor %}
        </div>
    </div>
</div>
<section class=\"imgWithText\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\" data-animation=\"slideFromLeft\">
                <img src=\"{{ imgText_2_img | media }}\" alt=\"\">
            </div>
            <div class=\"col\">
                <div class=\"textContent\">
                    <h3 class=\"title\" data-animation=\"slideFromRight\">
                        {{imgText_2_title}}
                    </h3>
                    <p class=\"text\" data-animation=\"slideFromRight\">
                        {{imgText_2_text}}
                    </p>
                    <a class=\"arrow_btn_bg\" href=\"kariera\" data-animation=\"slideFromRight\">Zobacz OFERTY PRACY</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"simpleWithBtn\">
    <div class=\"container\">
        <h3 class=\"title\" data-animation=\"fadeIn\">
            {{btnText_2_title}}
        </h3>
        <p class=\"text\" data-animation=\"fadeIn\">
            {{btnText_2_text}}
        </p>
        <a class=\"arrow_btn_bg\" href=\"kariera\" data-animation=\"fadeIn\">
            KARIERA W GRANBOARD
        </a>
    </div>
</section>
<script>
    const serviceList = Array.from(document.querySelectorAll('.serviceList_item'));
    const navLeft = document.querySelector('.navLeft');
    const navRight = document.querySelector('.navRight');

    serviceList.forEach(function (item) {
        item.addEventListener('click', function (e) {
            const activeIndex = e.target.closest('.serviceList_item').getAttribute('data-slide');
            set_activeSlide(activeIndex);
        });
    });
    navLeft.addEventListener('click', function () {
        let activeIndex = get_activeIndex();
        activeIndex = activeIndex - 1 < 0 ? serviceList.length - 1 : activeIndex - 1;
        set_activeSlide(activeIndex);
    });
    navRight.addEventListener('click', function () {
        let activeIndex = get_activeIndex();
        activeIndex = activeIndex + 1 > serviceList.length - 1 ? 0 : activeIndex + 1;
        set_activeSlide(activeIndex);
    });

    function get_activeIndex() {
        const slider = get_slideData();

        let activeIndex = -1;
        slider.forEach(function (el, index) {
            if (el.active)
                activeIndex = index;
        });
        return activeIndex;

    }

    function get_slideData() {
        const slide_list = Array.from(document.querySelectorAll('.serviceList_item'));
        const slide_content = Array.from(document.querySelectorAll('.contentSide_slide'));
        const slide_img = Array.from(document.querySelectorAll('.imgSide_item'));
        let data = [];
        slide_list.forEach(function (el, index) {

            data.push({
                slide_item: el,
                slide_content: slide_content[index],
                slide_img: slide_img[index],
                active: el.classList.contains('active')
            })
        });
        return data;
    }

    function set_activeSlide(activeIndex) {
        let slider = get_slideData();
        activeIndex = parseInt(activeIndex);
        if (!slider[activeIndex].active) {
            slider.forEach(function (el, index) {
                const slide_item = el.slide_item;
                const slide_content = el.slide_content;
                const slide_img = el.slide_img;
                if (index === activeIndex) {
                    slide_item.classList.add('active');
                    slide_content.classList.add('active');
                    slide_img.classList.add('active');
                    slide_img.active = true;
                } else {
                    slide_item.classList.remove('active');
                    slide_content.classList.remove('active');
                    slide_img.classList.remove('active');
                    slide_img.active = false;
                }
            });
        }
    }

</script>
{% partial 'footer' %}
</body>", "D:\\xampp\\htdocs\\granboard_cms_app/themes/grandboard/layouts/about.htm", "");
    }
}
