$(document).ready(function () {

    let winTop = $(window).scrollTop();
    let winHeight = window.innerHeight;
    console.log('winHeight', winHeight);
    let winBottom = winTop + winHeight;
    let lastScroll = winTop;
    let direction = 'firstView';

    check_isInView(winTop, winHeight, winBottom, lastScroll, direction);

    $(window).on('scroll', function () {
        winTop = $(document).scrollTop();
        winHeight = window.innerHeight;

        winBottom = winTop + winHeight;
        if (lastScroll > winTop) {
            direction = 'top';
        } else {
            direction = 'bottom';
        }
        check_isInView(winTop, winHeight, winBottom, lastScroll, direction)
        lastScroll = winTop;
    });
});

function check_isInView(winTop, winHeight, winBottom, lastScroll, direction) {
    const aniamtedElements = $('[data-animation]');
    $.each(aniamtedElements, function (index, el) {
        el = $(el);

        const elTop = el.offset().top;
        const elHeight = el.height();
        const elBottom = elTop + elHeight;

        switch (direction) {
            case 'firstView':
                console.log('winTop', winTop);
                console.log('winBottom', winBottom);
                console.log('elBottom', elBottom);
                console.log('elTop', elTop);
                console.log('                   ');
                if ((elBottom + 10 >= winTop && elBottom <= winBottom) || (elTop - 10 <= winBottom && elTop >= winTop)) {
                    el.addClass('is_view');
                }
                break;
            case 'top':
                if (elTop + elHeight / 2 >= winTop && elTop <= winBottom) {
                    el.addClass('is_view');
                }
                break;
            case 'bottom':
                if (elBottom - elHeight / 2 <= winBottom && elBottom >= winTop) {
                    el.addClass('is_view');
                }
                break;
        }
    });
}