<?php namespace Netsite\Serviceslist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNetsiteServiceslist extends Migration
{
    public function up()
    {
        Schema::create('netsite_serviceslist_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('img');
            $table->string('slug');
            $table->text('icon');
            $table->text('sub_name');
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('netsite_serviceslist_');
    }
}
