<?php namespace Netsite\Serviceslist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteServiceslist2 extends Migration
{
    public function up()
    {
        Schema::table('netsite_serviceslist_', function($table)
        {
            $table->smallInteger('about_description');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_serviceslist_', function($table)
        {
            $table->dropColumn('about_description');
        });
    }
}
