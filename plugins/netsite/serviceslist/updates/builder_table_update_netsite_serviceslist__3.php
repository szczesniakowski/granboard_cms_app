<?php namespace Netsite\Serviceslist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteServiceslist3 extends Migration
{
    public function up()
    {
        Schema::table('netsite_serviceslist_', function($table)
        {
            $table->text('about_description')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('netsite_serviceslist_', function($table)
        {
            $table->smallInteger('about_description')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
