<?php namespace Netsite\Serviceslist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteServiceslist extends Migration
{
    public function up()
    {
        Schema::table('netsite_serviceslist_', function($table)
        {
            $table->text('scope_of_service');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_serviceslist_', function($table)
        {
            $table->dropColumn('scope_of_service');
        });
    }
}
