<?php namespace Netsite\Serviceslist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteServiceslist6 extends Migration
{
    public function up()
    {
        Schema::table('netsite_serviceslist_', function($table)
        {
            $table->boolean('about_show');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_serviceslist_', function($table)
        {
            $table->dropColumn('about_show');
        });
    }
}
