<?php namespace Netsite\Serviceslist\Models;

use Model;

/**
 * Model
 */
class ServicesList extends Model
{
    use \October\Rain\Database\Traits\Validation;
        protected $jsonable = [
        'description',
        'scope_of_service',
    ];
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'netsite_serviceslist_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
