<?php namespace Netsite\Serviceslist;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Netsite\ServicesList\Components\ServicesList' => 'serviceslist',

        ];
    }

    public function registerSettings()
    {
    }
}
