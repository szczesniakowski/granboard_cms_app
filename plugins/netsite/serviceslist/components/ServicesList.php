<?php namespace Netsite\ServicesList\Components;

use Cms\Classes\ComponentBase;
use Netsite\ServicesList\Models\ServicesList as ModelServicesList;
use Session;
use Input;


class ServicesList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Serviceslist',
            'description' => 'Lista usług'
        ];
    }

    public function onRun()
    {
        $servicesList = ModelServicesList::paginate(20);
        $this->page['servicesList'] = $servicesList;

    }


}