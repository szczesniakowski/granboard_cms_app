<?php namespace Netsite\JobList\Components;

use Cms\Classes\ComponentBase;
use Netsite\JobList\Models\JobList as ModelProductList;
use Session;
use Input;


class JobList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'JobList',
            'description' => 'Lista ofert pracy'
        ];
    }

    public function getContent()
    {
        $jobList = ModelProductList::all();
        $this->page['jobList'] = $jobList;

    }


}