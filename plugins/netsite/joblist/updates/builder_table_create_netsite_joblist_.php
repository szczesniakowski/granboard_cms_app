<?php namespace Netsite\Joblist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNetsiteJoblist extends Migration
{
    public function up()
    {
        Schema::create('netsite_joblist_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('section');
            $table->string('location');
            $table->text('description');
            $table->text('expectations');
            $table->text('offer');
            $table->string('slug');
            $table->dateTime('date_start');
            $table->dateTime('data_end');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('netsite_joblist_');
    }
}
