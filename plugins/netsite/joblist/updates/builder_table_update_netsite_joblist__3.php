<?php namespace Netsite\Joblist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteJoblist3 extends Migration
{
    public function up()
    {
        Schema::table('netsite_joblist_', function($table)
        {
            $table->dropColumn('date_start');
            $table->dropColumn('date_end');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_joblist_', function($table)
        {
            $table->dateTime('date_start');
            $table->dateTime('date_end');
        });
    }
}
