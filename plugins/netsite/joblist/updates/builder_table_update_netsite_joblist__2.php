<?php namespace Netsite\Joblist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteJoblist2 extends Migration
{
    public function up()
    {
        Schema::table('netsite_joblist_', function($table)
        {
            $table->renameColumn('data_end', 'date_end');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_joblist_', function($table)
        {
            $table->renameColumn('date_end', 'data_end');
        });
    }
}
