<?php namespace Netsite\Joblist\Models;

use Model;

/**
 * Model
 */
class JobList extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $jsonable = [
        'expectations',
        'offer',
    ];

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'netsite_joblist_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
