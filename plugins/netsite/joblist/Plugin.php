<?php namespace Netsite\Joblist;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Netsite\JobList\Components\JobList' => 'joblist'
        ];
    }

    public function registerSettings()
    {
    }
}
