<?php namespace Netsite\Productlist;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Netsite\ProductList\Components\ProductList' => 'productlist',
            'Netsite\ProductList\Components\ProductList_static' => 'productList_static',

        ];
    }

    public function registerSettings()
    {
    }
}
