<?php namespace Netsite\Productlist\Models;

use Model;

/**
 * Model
 */
class ProductList extends Model
{
    use \October\Rain\Database\Traits\Validation;
    protected $jsonable = [
        'properties',
        'slider',
        'download',
    ];
    public $attachOne = [
        'fileupload1' => 'System\Models\File'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'netsite_productlist_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
