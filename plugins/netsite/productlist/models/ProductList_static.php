<?php namespace Netsite\Productlist\Models;

use Model;

/**
 * Model
 */
class ProductList_static extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'netsite_productlist_static';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
