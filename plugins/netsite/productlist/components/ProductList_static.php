<?php namespace Netsite\Productlist\Components;

use Cms\Classes\ComponentBase;
use Netsite\Productlist\Models\ProductList_static as ModelProductList_static;
use Session;
use Input;


class ProductList_static extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'ProductList_static',
            'description' => 'Lista Produktów - statyczna'
        ];
    }

    public function onRun()
    {
        $productList_static = ModelProductList_static::all();
        $this->page['ProductList_static'] = $productList_static;

    }


}