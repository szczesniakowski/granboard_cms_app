<?php namespace Netsite\Productlist\Components;

use Cms\Classes\ComponentBase;
use Netsite\Productlist\Models\ProductList as ModelProductList;
use Session;
use Input;


class ProductList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Productlist',
            'description' => 'Lista Produktów'
        ];
    }

    public function getContent()
    {
        $productList = ModelProductList::all();
//        $productList = ModelProductList::all();
        $this->page['productList'] = $productList;

    }


}