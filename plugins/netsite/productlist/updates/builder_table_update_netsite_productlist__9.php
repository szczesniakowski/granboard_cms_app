<?php namespace Netsite\Productlist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteProductlist9 extends Migration
{
    public function up()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->text('about_img');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->dropColumn('about_img');
        });
    }
}
