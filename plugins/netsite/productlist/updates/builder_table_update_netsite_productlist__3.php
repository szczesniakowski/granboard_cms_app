<?php namespace Netsite\Productlist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteProductlist3 extends Migration
{
    public function up()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->text('mainpage_img');
            $table->string('mainpage_expand_shortcut');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->dropColumn('mainpage_img');
            $table->dropColumn('mainpage_expand_shortcut');
        });
    }
}
