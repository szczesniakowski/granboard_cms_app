<?php namespace Netsite\Productlist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteProductlist extends Migration
{
    public function up()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->dropColumn('id');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->integer('id')->unsigned();
        });
    }
}
