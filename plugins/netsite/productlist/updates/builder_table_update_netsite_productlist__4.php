<?php namespace Netsite\Productlist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteProductlist4 extends Migration
{
    public function up()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->boolean('mainpage_show');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->dropColumn('mainpage_show');
        });
    }
}
