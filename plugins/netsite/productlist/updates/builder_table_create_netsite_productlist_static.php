<?php namespace Netsite\Productlist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNetsiteProductlistStatic extends Migration
{
    public function up()
    {
        Schema::create('netsite_productlist_static', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('about_product_img');
            $table->string('about_product_text');
            $table->text('img_with_text_img');
            $table->string('img_with_text_title');
            $table->text('img_with_text_text');
            $table->string('order_title');
            $table->text('order_text');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('netsite_productlist_static');
    }
}
