<?php namespace Netsite\Productlist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNetsiteProductlist extends Migration
{
    public function up()
    {
        Schema::create('netsite_productlist_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id')->unsigned();
            $table->string('name');
            $table->string('description');
            $table->text('main_img');
            $table->text('properties');
            $table->text('slider');
            $table->text('technical_data');
            $table->text('description_2');
            $table->text('img_with_text');
            $table->text('download');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('netsite_productlist_');
    }
}
