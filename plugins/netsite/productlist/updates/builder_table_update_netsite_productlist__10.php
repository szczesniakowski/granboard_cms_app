<?php namespace Netsite\Productlist\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNetsiteProductlist10 extends Migration
{
    public function up()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->text('about_text');
        });
    }
    
    public function down()
    {
        Schema::table('netsite_productlist_', function($table)
        {
            $table->dropColumn('about_text');
        });
    }
}
